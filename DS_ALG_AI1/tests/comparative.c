// comparative.c
// Toni Barella
// Algoritmos & Inteligencia Artificial
// ESAT 2016-2019
//
// File for comparative of ADTs
#include <windows.h>
#include "ABGS_MemoryManager/abgs_memory_manager.h"
#include "adt_list.h"
#include "adt_vector.h"
#include "adt_dllist.h"

#include ".\..\tests\test_base.c"

#define kRepetitions 1000
#define kElementsNumber 100
#define kDataSize 1

void* data[kElementsNumber];

struct avg_times {
	double v_avg_time[4];
	double l_avg_time[5];
	double dll_avg_time[5];
}Times;

List *test_list;
DLList *test_dllist;
Vector *test_vector; 

void TESTBASE_generateDataForComparative( )
{
	for (int i = 0; i < kElementsNumber; ++i) {
		//data[i] = TESTBASE_generateDataIntegerAsString(i);
		//data[i] = TESTBASE_allocateData(dataSize);
		data[i] = MM->malloc(kDataSize);
	}
}

void TESTBASE_deleteData() {
	for (int i = 0; i < kElementsNumber; ++i) {
		if(NULL != data[i])
			MM->free(data[i]);
	}
}

void calculateTimeForVector()
{
	LARGE_INTEGER frequency;				// ticks per second
	LARGE_INTEGER  time_start, time_end;    // ticks in interval
	double elapsed_time = 0.0f;
	double total_time = 0.0f;

	


	///////////////////////////////////////////////////////////////////////
	// Frequency: ticks per second
	QueryPerformanceFrequency(&frequency);
	///////////////////////////////////////////////////////////////////////

	printf("##### TEST VECTOR #####\n");

	printf("-----Test insert-----\n");	

	printf("\n--Insert First--\n");

	for (u32 rep = 0; rep < kRepetitions; ++rep) {
		TESTBASE_generateDataForComparative();
		QueryPerformanceCounter(&time_start);
		// execute function to meassure 'repetitions' times
		for (u32 i = 0; i < kElementsNumber; ++i)
		{
			test_vector->ops_->insertFirst(test_vector, data[i], kDataSize);
			data[i] = NULL;

		}
		QueryPerformanceCounter(&time_end);
		elapsed_time = (time_end.QuadPart - time_start.QuadPart) * 1000000.0f / frequency.QuadPart;
		elapsed_time /= kElementsNumber;
		total_time += elapsed_time;
		test_vector->ops_->reset(test_vector);

		printf("Time: %f\n", elapsed_time);
	}

	

	printf("\n\nTime: %f\n", total_time);
	Times.v_avg_time[1] = total_time / kRepetitions;
	printf("\nAverage Time: %f\n", Times.v_avg_time[1]);
	
	total_time = 0.0f;
	elapsed_time = 0.0f;
	QueryPerformanceFrequency(&frequency);
	printf("--Insert Last--\n");
	
	for (u32 rep = 0; rep < kRepetitions; ++rep) {
		TESTBASE_generateDataForComparative();
		QueryPerformanceCounter(&time_start);
		// execute function to meassure 'repetitions' times
		for (u32 i = 0; i < kElementsNumber; ++i)
		{
			test_vector->ops_->insertLast(test_vector, data[i], kDataSize);
			data[i] = NULL;

		}
		QueryPerformanceCounter(&time_end);
		elapsed_time = (time_end.QuadPart - time_start.QuadPart) * 1000000.0f / frequency.QuadPart;
		elapsed_time /= kElementsNumber;
		total_time += elapsed_time;
		test_vector->ops_->reset(test_vector);

		printf("Time: %f\n", elapsed_time);
	}


	printf("\n\nTime: %f\n", total_time);
	Times.v_avg_time[0] = total_time / kRepetitions;
	printf("\nAverage Time: %f\n", Times.v_avg_time[0]);



	total_time = 0.0f;

	printf("\n--Insert At 50--\n");

	for (u32 rep = 0; rep < kRepetitions; ++rep) {
		TESTBASE_generateDataForComparative();
		QueryPerformanceCounter(&time_start);
		// execute function to meassure 'repetitions' times
		for (u32 i = 0; i < kElementsNumber; ++i)
		{
			test_vector->ops_->insertAt(test_vector, data[i], rand() % 100, kDataSize);
			data[i] = NULL;
			// execute function
		}

		// stop timer
		QueryPerformanceCounter(&time_end);
		///////////////////////////////////////////////////////////////////////
		// compute the elapsed time in microseconds
		elapsed_time = (time_end.QuadPart - time_start.QuadPart) * 1000000.0f / frequency.QuadPart;
		///////////////////////////////////////////////////////////////////////
		// compute the average time
		elapsed_time /= kElementsNumber;
		total_time += elapsed_time;
		///////////////////////////////////////////////////////////////////////
		test_vector->ops_->reset(test_vector);
		///////////////////////////////////////////////////////////////////////

		printf("Time: %f\n", elapsed_time);
	}

	printf("\n\nTime: %f\n", total_time);
	Times.v_avg_time[2] = total_time / kRepetitions;
	printf("\nAverage Time: %f\n", Times.v_avg_time[2]);

	total_time = 0.0f;

	printf("-----Test extract-----\n");
	printf("--Extract first--\n");
	
	TESTBASE_generateDataForComparative();
	
	for (u32 rep = 0; rep < kRepetitions; ++rep) {
		
		for (u32 i = 0; i < kElementsNumber; ++i)
		{
			test_vector->ops_->insertLast(test_vector,data[i],kDataSize);

			// execute function
		}
		QueryPerformanceCounter(&time_start);
		// execute function to meassure 'repetitions' times
		for (u32 i = 0; i < kElementsNumber; ++i)
		{
			data[i] = test_vector->ops_->extractFirst(test_vector);
			 
			// execute function
		}

		// stop timer
		QueryPerformanceCounter(&time_end);
		///////////////////////////////////////////////////////////////////////
		// compute the elapsed time in microseconds
		elapsed_time = (time_end.QuadPart - time_start.QuadPart) * 1000000.0f / frequency.QuadPart;
		///////////////////////////////////////////////////////////////////////
		// compute the average time
		elapsed_time /= kElementsNumber;
		total_time += elapsed_time;
		///////////////////////////////////////////////////////////////////////
		//test_vector->ops_->reset(test_vector);
		///////////////////////////////////////////////////////////////////////

		printf("Time: %f\n", elapsed_time);
	}

	printf("\n\nTime: %f\n", total_time);
	Times.v_avg_time[3] = total_time / kRepetitions;
	printf("\nAverage Time: %f\n", Times.v_avg_time[3]);

	TESTBASE_deleteData();
	
}

void calculateTimeForList()
{
	LARGE_INTEGER frequency;				// ticks per second
	LARGE_INTEGER  time_start, time_end;    // ticks in interval
	double elapsed_time = 0.0f;
	double total_time = 0.0f; 

	printf("\n###### LIST TEST ######\n");


	///////////////////////////////////////////////////////////////////////
	// Frequency: ticks per second
	QueryPerformanceFrequency(&frequency);
	///////////////////////////////////////////////////////////////////////

	printf("-----Test insert-----\n");


	printf("\n--Insert First--\n");

	for (u32 rep = 0; rep < kRepetitions; ++rep) {
		TESTBASE_generateDataForComparative();
		QueryPerformanceCounter(&time_start);
		for (u32 i = 0; i < kElementsNumber; ++i)
		{
			test_list->ops_->insertFirst(test_list, data[i], kDataSize);
			data[i] = NULL;
			// execute function
		}

		// stop timer
		QueryPerformanceCounter(&time_end);
		elapsed_time = (time_end.QuadPart - time_start.QuadPart) * 1000000.0f / frequency.QuadPart;
		elapsed_time /= kElementsNumber;
		total_time += elapsed_time;
		test_list->ops_->reset(test_list);
		printf("Time: %f\n", elapsed_time);
	}

	printf("\n\nTime: %f\n", total_time);
	Times.l_avg_time[1] = total_time / kRepetitions;
	printf("\nAverage Time: %f\n", Times.l_avg_time[1]);

	total_time = 0.0f;

	printf("--Insert Last--\n");

	for (u32 rep = 0; rep < kRepetitions; ++rep) {
		TESTBASE_generateDataForComparative();
		QueryPerformanceCounter(&time_start);
		// execute function to meassure 'repetitions' times
		for (u32 i = 0; i < kElementsNumber; ++i)
		{
			test_list->ops_->insertLast(test_list, data[i], kDataSize);
			data[i] = NULL;
			// execute function
		}

		// stop timer
		QueryPerformanceCounter(&time_end);
		///////////////////////////////////////////////////////////////////////
		// compute the elapsed time in microseconds
		elapsed_time = (time_end.QuadPart - time_start.QuadPart) * 1000000.0f / frequency.QuadPart;
		///////////////////////////////////////////////////////////////////////
		// compute the average time
		elapsed_time /= kElementsNumber;
		total_time += elapsed_time;
		///////////////////////////////////////////////////////////////////////
		test_list->ops_->reset(test_list);
		///////////////////////////////////////////////////////////////////////

		printf("Time: %f\n", elapsed_time);
	}

	printf("\n\nTime: %f\n", total_time);
	Times.l_avg_time[0] = total_time / kRepetitions;
	printf("\nAverage Time: %f\n", Times.l_avg_time[0]);

	total_time = 0.0f;

	

	printf("\n--Insert At 50--\n");

	for (u32 rep = 0; rep < kRepetitions; ++rep) {
		TESTBASE_generateDataForComparative();
		QueryPerformanceCounter(&time_start);
		// execute function to meassure 'repetitions' times
		for (u32 i = 0; i < kElementsNumber; ++i)
		{
			test_list->ops_->insertAt(test_list, data[i], rand() % 100, kDataSize);
			data[i] = NULL;
			// execute function
		}

		// stop timer
		QueryPerformanceCounter(&time_end);
		///////////////////////////////////////////////////////////////////////
		// compute the elapsed time in microseconds
		elapsed_time = (time_end.QuadPart - time_start.QuadPart) * 1000000.0f / frequency.QuadPart;
		///////////////////////////////////////////////////////////////////////
		// compute the average time
		elapsed_time /= kElementsNumber;
		total_time += elapsed_time;
		///////////////////////////////////////////////////////////////////////
		test_list->ops_->reset(test_list);
		///////////////////////////////////////////////////////////////////////

		printf("Time: %f\n", elapsed_time);
	}

	printf("\n\nTime: %f\n", total_time);
	Times.l_avg_time[2] = total_time / kRepetitions;
	printf("\nAverage Time: %f\n", Times.l_avg_time[2]);
	
	total_time = 0.0f;

	printf("-----Test extract-----\n");
	printf("--Extract Last--\n");

	TESTBASE_generateDataForComparative();

	for (u32 rep = 0; rep < kRepetitions; ++rep) {

		for (u32 i = 0; i < kElementsNumber; ++i)
		{
			test_list->ops_->insertLast(test_list, data[i], kDataSize);

			// execute function
		}
		QueryPerformanceCounter(&time_start);
		// execute function to meassure 'repetitions' times
		for (u32 i = 0; i < kElementsNumber; ++i)
		{
			data[i] = test_list->ops_->extractLast(test_list);

			// execute function
		}

		// stop timer
		QueryPerformanceCounter(&time_end);
		///////////////////////////////////////////////////////////////////////
		// compute the elapsed time in microseconds
		elapsed_time = (time_end.QuadPart - time_start.QuadPart) * 1000000.0f / frequency.QuadPart;
		///////////////////////////////////////////////////////////////////////
		// compute the average time
		elapsed_time /= kElementsNumber;
		total_time += elapsed_time;
		///////////////////////////////////////////////////////////////////////
		//test_vector->ops_->reset(test_vector);
		///////////////////////////////////////////////////////////////////////

		printf("Time: %f\n", elapsed_time);
	}

	printf("\n\nTime: %f\n", total_time);
	Times.l_avg_time[3] = total_time / kRepetitions;
	printf("\nAverage Time: %f\n", Times.l_avg_time[3]);
	
	total_time = 0.0f;
	
	printf("--Extract First--\n");

	//TESTBASE_generateDataForComparative();

	for (u32 rep = 0; rep < kRepetitions; ++rep) {

		for (u32 i = 0; i < kElementsNumber; ++i)
		{
			test_list->ops_->insertLast(test_list, data[i], kDataSize);

			// execute function
		}
		QueryPerformanceCounter(&time_start);
		// execute function to meassure 'repetitions' times
		for (u32 i = 0; i < kElementsNumber; ++i)
		{
			data[i] = test_list->ops_->extractFirst(test_list);

			// execute function
		}

		// stop timer
		QueryPerformanceCounter(&time_end);
		///////////////////////////////////////////////////////////////////////
		// compute the elapsed time in microseconds
		elapsed_time = (time_end.QuadPart - time_start.QuadPart) * 1000000.0f / frequency.QuadPart;
		///////////////////////////////////////////////////////////////////////
		// compute the average time
		elapsed_time /= kElementsNumber;
		total_time += elapsed_time;
		///////////////////////////////////////////////////////////////////////
		//test_vector->ops_->reset(test_vector);
		///////////////////////////////////////////////////////////////////////

		printf("Time: %f\n", elapsed_time);
	}

	printf("\n\nTime: %f\n", total_time);
	Times.l_avg_time[4] = total_time / kRepetitions;
	printf("\nAverage Time: %f\n", Times.l_avg_time[4]);

	TESTBASE_deleteData();
	
}

void calculateTimeForDLList()
{
	LARGE_INTEGER frequency;				// ticks per second
	LARGE_INTEGER  time_start, time_end;    // ticks in interval
	double elapsed_time = 0.0f;
	double total_time = 0.0f;


	///////////////////////////////////////////////////////////////////////
	// Frequency: ticks per second
	QueryPerformanceFrequency(&frequency);
	///////////////////////////////////////////////////////////////////////

	printf("\n##### TEST DLLIST #####\n");

	printf("-----Test insert-----\n");

	printf("--Insert Last--\n");

	for (u32 rep = 0; rep < kRepetitions; ++rep) {
		TESTBASE_generateDataForComparative();
		QueryPerformanceCounter(&time_start);
		// execute function to meassure 'repetitions' times
		for (u32 i = 0; i < kElementsNumber; ++i)
		{
			test_dllist->ops_->insertLast(test_dllist, data[i], kDataSize);
			data[i] = NULL;
		}
		
		QueryPerformanceCounter(&time_end);
		elapsed_time = (time_end.QuadPart - time_start.QuadPart) * 1000000.0f / frequency.QuadPart;
		elapsed_time /= kElementsNumber;
		total_time += elapsed_time;
		test_dllist->ops_->reset(test_dllist);

		printf("Time: %f\n", elapsed_time);
	}

	printf("\n\nTime: %f\n", total_time);
	Times.dll_avg_time[0] = total_time / kRepetitions;
	printf("\nAverage Time: %f\n", Times.dll_avg_time[0]);

	total_time = 0.0f;

	printf("\n--Insert First--\n");

	for (u32 rep = 0; rep < kRepetitions; ++rep) {
		TESTBASE_generateDataForComparative();
		QueryPerformanceCounter(&time_start);
		// execute function to meassure 'repetitions' times
		for (u32 i = 0; i < kElementsNumber; ++i)
		{
			test_dllist->ops_->insertFirst(test_dllist, data[i], kDataSize);
			data[i] = NULL;
			// execute function
		}

		// stop timer
		QueryPerformanceCounter(&time_end);
		///////////////////////////////////////////////////////////////////////
		// compute the elapsed time in microseconds
		elapsed_time = (time_end.QuadPart - time_start.QuadPart) * 1000000.0f / frequency.QuadPart;
		///////////////////////////////////////////////////////////////////////
		// compute the average time
		elapsed_time /= kElementsNumber;
		total_time += elapsed_time;
		///////////////////////////////////////////////////////////////////////
		test_dllist->ops_->reset(test_dllist);
		///////////////////////////////////////////////////////////////////////

		printf("Time: %f\n", elapsed_time);
	}

	printf("\n\nTime: %f\n", total_time);
	Times.dll_avg_time[1] = total_time / kRepetitions;
	printf("\nAverage Time: %f\n", Times.dll_avg_time[1]);

	total_time = 0.0f;

	printf("\n--Insert At 50--\n");

	for (u32 rep = 0; rep < kRepetitions; ++rep) {
		TESTBASE_generateDataForComparative();
		QueryPerformanceCounter(&time_start);
		// execute function to meassure 'repetitions' times
		for (u32 i = 0; i < kElementsNumber; ++i)
		{
			test_dllist->ops_->insertAt(test_dllist, data[i], rand() % 100, kDataSize);
			data[i] = NULL;
			// execute function
		}

		// stop timer
		QueryPerformanceCounter(&time_end);
		///////////////////////////////////////////////////////////////////////
		// compute the elapsed time in microseconds
		elapsed_time = (time_end.QuadPart - time_start.QuadPart) * 1000000.0f / frequency.QuadPart;
		///////////////////////////////////////////////////////////////////////
		// compute the average time
		elapsed_time /= kElementsNumber;
		total_time += elapsed_time;
		///////////////////////////////////////////////////////////////////////
		test_dllist->ops_->reset(test_dllist);
		///////////////////////////////////////////////////////////////////////

		printf("Time: %f\n", elapsed_time);
	}

	printf("\n\nTime: %f\n", total_time);
	Times.dll_avg_time[2] = total_time / kRepetitions;
	printf("\nAverage Time: %f\n", Times.dll_avg_time[2]);

	total_time = 0.0f;

	printf("-----Test extract-----\n");
	printf("--Extract first--\n");

	TESTBASE_generateDataForComparative();

	for (u32 rep = 0; rep < kRepetitions; ++rep) {

		for (u32 i = 0; i < kElementsNumber; ++i)
		{
			test_dllist->ops_->insertLast(test_dllist, data[i], kDataSize);

			// execute function
		}
		QueryPerformanceCounter(&time_start);
		// execute function to meassure 'repetitions' times
		for (u32 i = 0; i < kElementsNumber; ++i)
		{
			data[i] = test_dllist->ops_->extractFirst(test_dllist);

			// execute function
		}

		// stop timer
		QueryPerformanceCounter(&time_end);
		///////////////////////////////////////////////////////////////////////
		// compute the elapsed time in microseconds
		elapsed_time = (time_end.QuadPart - time_start.QuadPart) * 1000000.0f / frequency.QuadPart;
		///////////////////////////////////////////////////////////////////////
		// compute the average time
		elapsed_time /= kElementsNumber;
		total_time += elapsed_time;
		///////////////////////////////////////////////////////////////////////
		//test_vector->ops_->reset(test_vector);
		///////////////////////////////////////////////////////////////////////

		printf("Time: %f\n", elapsed_time);
	}

	printf("\n\nTime: %f\n", total_time);
	Times.dll_avg_time[3] = total_time / kRepetitions;
	printf("\nAverage Time: %f\n", Times.dll_avg_time[3]);

	total_time = 0.0f;

	printf("-----Test extract-----\n");
	printf("--Extract Last--\n");

	//TESTBASE_generateDataForComparative();

	for (u32 rep = 0; rep < kRepetitions; ++rep) {

		for (u32 i = 0; i < kElementsNumber; ++i)
		{
			test_dllist->ops_->insertLast(test_dllist, data[i], kDataSize);

			// execute function
		}
		QueryPerformanceCounter(&time_start);
		// execute function to meassure 'repetitions' times
		for (u32 i = 0; i < kElementsNumber; ++i)
		{
			data[i] = test_dllist->ops_->extractLast(test_dllist);

			// execute function
		}

		// stop timer
		QueryPerformanceCounter(&time_end);
		///////////////////////////////////////////////////////////////////////
		// compute the elapsed time in microseconds
		elapsed_time = (time_end.QuadPart - time_start.QuadPart) * 1000000.0f / frequency.QuadPart;
		///////////////////////////////////////////////////////////////////////
		// compute the average time
		elapsed_time /= kElementsNumber;
		total_time += elapsed_time;
		///////////////////////////////////////////////////////////////////////
		//test_vector->ops_->reset(test_vector);
		///////////////////////////////////////////////////////////////////////

		printf("Time: %f\n", elapsed_time);
	}

	printf("\n\nTime: %f\n", total_time);
	Times.dll_avg_time[4] = total_time / kRepetitions;
	printf("\nAverage Time: %f\n", Times.dll_avg_time[4]);

}



int main()
{
	srand(time(NULL));

	test_list = LIST_create(1);
	test_dllist = DLLIST_create(1);
	test_vector = VECTOR_create(kElementsNumber);

	

	calculateTimeForVector();
	calculateTimeForList();
	calculateTimeForDLList();

	printf("Vector results:\n");
	printf("\t Insert Last: %f\n",Times.v_avg_time[0]);
	printf("\t Insert First: %f\n", Times.v_avg_time[1]);
	printf("\t Insert At random: %f\n", Times.v_avg_time[2]);
	printf("\t Extract First: %f\n", Times.v_avg_time[3]);

	printf("\nList results:\n");
	printf("\t Insert Last: %f\n", Times.l_avg_time[0]);
	printf("\t Insert First: %f\n", Times.l_avg_time[1]);
	printf("\t Insert At random: %f\n", Times.l_avg_time[2]);
	printf("\t Extract Last: %f\n", Times.l_avg_time[3]);
	printf("\t Extract First: %f\n", Times.l_avg_time[4]);

	printf("\nDLList results:\n");
	printf("\t Insert Last: %f\n", Times.dll_avg_time[0]);
	printf("\t Insert First: %f\n", Times.dll_avg_time[1]);
	printf("\t Insert At random: %f\n", Times.dll_avg_time[2]);
	printf("\t Extract Last: %f\n", Times.dll_avg_time[3]);
	printf("\t Extract First: %f\n", Times.dll_avg_time[4]);
	
	MM->status();

	
	test_list->ops_->destroy(&test_list);
	test_dllist->ops_->destroy(&test_dllist);
	test_vector->ops_->destroy(&test_vector);
	TESTBASE_deleteData();
	
	MM->status();

	MM->destroy();
	printf("Press ENTER to continue\n");
	getchar();
	return 0;
}