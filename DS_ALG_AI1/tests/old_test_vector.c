// test_memory_node.c
// Toni Barella
// Algoritmos & Inteligencia Artificial
// ESAT 2016-2019
//
//Test battery for memory_node

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "adt_vector.h"


const u16 kSizeData1 = (sizeof(u8) * 5);
const u16 kSizeData2 = (sizeof(u8) * 15);
const u16 kSizeData3 = (sizeof(u8) * 10);
const u16 kSizeData4 = (sizeof(u8) * 26);
const u16 kSizeData5 = (sizeof(u8) * 7);
const u16 kSizeData6 = (sizeof(u8) * 18);


void printFunctionResult(u8 *msg, s16 error_type) {
	u8 *error_msg = malloc(sizeof(u8) * 100);
	if (NULL == error_msg)
	{
		printf(" [TEST] Error allocating error mesage\n");
		return;
	}
	printf(" [TEST] Function %s returns ", msg);
	switch (error_type)
	{
	case 0:
		printf("OK");
		break;
	default:
		strcpy((char *)error_msg, "");
		printf("FAIL with error %d (%s)", error_type, error_msg);
		break;
	}
	//printf(" in address = %p", mn);
	printf("\n");
	free(error_msg);
}

void printData() {

}


int main()
{
	Vector *my_vector = VECTOR_Create(5);
	u8 *ptr_data_1 = (u8*)malloc(sizeof(u8) * kSizeData1);
	u8 *ptr_data_2 = (u8*)malloc(sizeof(u8) * kSizeData2);
	u8 *ptr_data_3 = (u8*)malloc(sizeof(u8) * kSizeData3);
	u8 *ptr_data_4 = (u8*)malloc(sizeof(u8) * kSizeData4);
	u8 *ptr_data_5 = (u8*)malloc(sizeof(u8) * kSizeData5);
	u8 *ptr_data_6 = (u8*)malloc(sizeof(u8) * kSizeData6);

	for (u8 i = 0; i < kSizeData1; ++i)
	{
		*(ptr_data_1 + i) = 'a' + i;
	}
	for (u8 i = 0; i < kSizeData2; ++i)
	{
		*(ptr_data_2 + i) = 'f' + i;
	}
	for (u8 i = 0; i < kSizeData3; ++i)
	{
		*(ptr_data_3 + i) = '0' + i;
	}
	for (u8 i = 0; i < kSizeData4; ++i)
	{
		*(ptr_data_4 + i) = 'A' + i;
	}
	for (u8 i = 0; i < kSizeData5; ++i)
	{
		*(ptr_data_5 + i) = 'd' + i;
	}
	for (u8 i = 0; i < kSizeData6; ++i)
	{
		*(ptr_data_6 + i) = '5' + i;
	}

	my_vector->ops_->insertLast(my_vector, ptr_data_1, kSizeData1);
	my_vector->ops_->insertLast(my_vector, ptr_data_2, kSizeData2);
	my_vector->ops_->insertLast(my_vector, ptr_data_3, kSizeData3);

	my_vector->ops_->print(my_vector);

	ptr_data_1 = my_vector->ops_->extractFirst(my_vector);

	my_vector->ops_->print(my_vector);
	my_vector->ops_->insertFirst(my_vector, ptr_data_1, kSizeData1);
	my_vector->ops_->print(my_vector);

	ptr_data_3 = my_vector->ops_->extractLast(my_vector);
	my_vector->ops_->print(my_vector);
	my_vector->ops_->insertAt(my_vector, ptr_data_3, 2, kSizeData3);
	my_vector->ops_->print(my_vector);
	my_vector->ops_->insertAt(my_vector, ptr_data_4, 2, kSizeData4);
	my_vector->ops_->print(my_vector);

	ptr_data_3 = my_vector->ops_->extractLast(my_vector);
	my_vector->ops_->print(my_vector);

	my_vector->ops_->insertAt(my_vector, ptr_data_3, 0, kSizeData3);
	my_vector->ops_->print(my_vector);

	ptr_data_3 = my_vector->ops_->extractFirst(my_vector);
	my_vector->ops_->print(my_vector);

	my_vector->ops_->insertLast(my_vector, ptr_data_3, kSizeData3);
	my_vector->ops_->print(my_vector);
	
	my_vector->ops_->insertAt(my_vector, ptr_data_5, 4, kSizeData5);
	my_vector->ops_->print(my_vector);

	printFunctionResult( "Insert first" , my_vector->ops_->insertFirst(my_vector, ptr_data_6, kSizeData6));
	my_vector->ops_->print(my_vector);

	printFunctionResult("Insert last", my_vector->ops_->insertLast(my_vector, ptr_data_6, kSizeData6));
	my_vector->ops_->print(my_vector);

	ptr_data_2 = my_vector->ops_->extractAt(my_vector, 1);
	my_vector->ops_->print(my_vector);

	my_vector->ops_->extractAt(my_vector, 2);
	my_vector->ops_->print(my_vector);

	printFunctionResult("Insert first", my_vector->ops_->insertFirst(my_vector, ptr_data_2, kSizeData2));
	my_vector->ops_->print(my_vector);

	printFunctionResult("Resize", my_vector->ops_->resize(my_vector, 10));
	my_vector->ops_->print(my_vector);

	return 0;
}
