// test_memory_node.h
// Toni Barella
// Algoritmos & Inteligencia Artificial
// ESAT 2016-2019
//
//Test battery for memory_node

#include <stdio.h>
#include <stdlib.h>

#include "adt_memory_node.h"

int main()
{
  //node created to always have a reference to the operations
  MemoryNode *aux_for_ops = NULL;

  MemoryNode *node = NULL;
  MemoryNode *node2 = NULL;
  node = MEMNODE_create();
  if(NULL == node){
    printf("\n create returned a null node");
    return 1;
  }

  node2 = MEMNODE_create();
  if(NULL == node2){
    printf("\n create returned a null node");
    return 1;
  }
  aux_for_ops = MEMNODE_create();
  
  if (NULL == aux_for_ops) {
	  printf("\n create returned a null node");
	  return 1;
  }

  u8 *src1 = malloc(sizeof(u8));
  u8 *src2 = malloc(sizeof(u8));
  u8 *src3 = malloc(sizeof(u8));
  
  //char src3;

  *src1 = 1;
  *src2 = 2;
  *src3 = 3;
  

  //printf("%d, %d", sizeof(*src1), sizeof(*src2));
  
  aux_for_ops->ops_->setData(node, (void*)src1, 1);
  aux_for_ops->ops_->print(node);

  aux_for_ops->ops_->memCopy(node, (void*)src2, 1);
  aux_for_ops->ops_->print(node);
  
  aux_for_ops->ops_->memConcat(node, (void*)src3, 1);
  aux_for_ops->ops_->print(node);

  aux_for_ops->ops_->memMask(node, 0x0);
  aux_for_ops->ops_->print(node);
  
  aux_for_ops->ops_->setData(node2, (void*)src2, 1);
  aux_for_ops->ops_->print(node2);

  aux_for_ops->ops_->memSet(node2, 0x1);
  aux_for_ops->ops_->print(node2);

  void *aux_data = aux_for_ops->ops_->data(node);
  u16 aux_size = aux_for_ops->ops_->size(node);
  
  unsigned char *p = aux_data;

  for (int i = 0; i < node->size_; i++) {
	  printf("%X", p[i]);
  }
  printf("%d\n", aux_size);


  
  aux_for_ops->ops_->setData(node2, aux_data, aux_size); //heap error, but work if ignored
  aux_for_ops->ops_->print(node2);

  

  free(src1);
  free(src2);
  free(src3);
  //free(src4);  //heap crash when i tried to free this pointer, sometimes 

  free(node);
  free(node2);
  
  return 0;
}
