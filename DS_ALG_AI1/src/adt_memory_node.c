
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "common_def.h"
#include "adt_memory_node.h"
#include "ABGS_MemoryManager/abgs_memory_manager.h"

// Memory Node Declarations
//static s16 MEMNODE_initWithoutCheck(MemoryNode *node);	// inits a MN with no checks
//static void* MEMNODE_data(MemoryNode *node);	// returns a reference to data_
//static u16 MEMNODE_size(MemoryNode *node);		// returns data size


static void* MEMNODE_data(MemoryNode *node);
static u16 MEMNODE_size(MemoryNode *node);
static s16 MEMNODE_setData(MemoryNode *node, void *src, u16 bytes);
static s16 MEMNODE_reset(MemoryNode *node);
static s16 MEMNODE_softReset(MemoryNode *node);
static s16 MEMNODE_free(MemoryNode *node);
static s16 MEMNODE_softFree(MemoryNode *node);
static s16 MEMNODE_setNext(MemoryNode *node, MemoryNode **next);
static s16 MEMNODE_setPrev(MemoryNode *node, MemoryNode **prev);
static MemoryNode* MEMNODE_next(MemoryNode *node);
static MemoryNode* MEMNODE_prev(MemoryNode *node);
static s16 MEMNODE_memSet(MemoryNode *node, u8 value);
static s16 MEMNODE_memCopy(MemoryNode *node, void *src, u16 bytes);
static s16 MEMNODE_memConcat(MemoryNode *node, void *src, u16 bytes);
static s16 MEMNODE_memMask(MemoryNode *node, u8 mask);
static s16 MEMNODE_createLite(MemoryNode *node); // Creates a memory node without memory allocation
static void MEMNODE_print(MemoryNode *node);

// Memory Node's API Definitions
struct memory_node_ops_s memory_node_ops =
{
	.data = MEMNODE_data,
	.size = MEMNODE_size,
	.setData = MEMNODE_setData,

	.reset = MEMNODE_reset,
	.softReset = MEMNODE_softReset,
	.free = MEMNODE_free,
	.softFree = MEMNODE_softFree,

	.setNext = MEMNODE_setNext,
	.setPrev = MEMNODE_setPrev,

	.next = MEMNODE_next,
	.prev = MEMNODE_prev,

	.memSet = MEMNODE_memSet,
	.memCopy = MEMNODE_memCopy,
	.memConcat = MEMNODE_memConcat,
	.memMask = MEMNODE_memMask,

	.print = MEMNODE_print
};

// Memory Node Definitions
MemoryNode* MEMNODE_create()
{
	MemoryNode *node = MM->malloc(sizeof(MemoryNode));
	if (node == NULL)
	{
#ifdef VERBOSE_
		printf("Error: [%s] not enough memory available\n", __FUNCTION__);
#endif
		return NULL;
	}
	MEMNODE_initWithoutCheck(node);
	return node;
}

s16 MEMNODE_createFromRef(MemoryNode **node)
{
	if (NULL == node) {
		return kErrorCode_Memory;
	}
	*node = MEMNODE_create();
	if (*node == NULL)
	{
#ifdef VERBOSE_
		printf("Error: [%s] not enough memory available\n", __FUNCTION__);
#endif
		return kErrorCode_Node;
	}
	return kErrorCode_Ok;
}

s16 MEMNODE_initWithoutCheck(MemoryNode *node)
{

	if (NULL == node) {
		return kErrorCode_Memory;
	}

	node->data_ = NULL;
	node->prev_ = NULL;
	node->next_ = NULL;
	node->size_ = 0;
	node->ops_ = &memory_node_ops;
	return kErrorCode_Ok;
}

void* MEMNODE_data(MemoryNode *node) // returns a reference to data_
{
	if (NULL != node) {
		if (NULL != node->data_)
			return node->data_;
		else {
			printf("Uninitialized data\n");
			return NULL;
		}
	}		
	else {
		printf("Uninitialized node\n");
		return NULL;
	}
		
}

u16	MEMNODE_size(MemoryNode *node) // returns data size
{
	if (NULL != node)
		
		return node->size_;
		
	else {
		printf("NULL node\n");
		return 0;
	}

}

s16 MEMNODE_setData(MemoryNode *node, void *src, u16 bytes)
{
	if (NULL == node) {
		return kErrorCode_Node;
	}

	if (NULL == src) {
		return kErrorCode_Source;
	}

	if (NULL != node->data_) {
		MM->free(node->data_);
	}
		
	node->data_ = src;
	node->size_ = bytes;
		
	return kErrorCode_Ok;
	
}

s16 MEMNODE_reset(MemoryNode *node) {
	if (NULL != node) {
		if (NULL != node->data_) {
			MM->free(node->data_);
		}
		node->next_ = NULL;
		node->prev_ = NULL;
		node->data_ = NULL;
		node->size_ = 0;
		return kErrorCode_Ok;
	}else
		return kErrorCode_Node;
}

s16 MEMNODE_softReset(MemoryNode *node) {
	if (NULL != node) {
		node->next_ = NULL;
		node->prev_ = NULL;
		node->data_ = NULL;
		node->size_ = 0;
		return kErrorCode_Ok;
	}
	else
		return kErrorCode_Node;
}

s16 MEMNODE_free(MemoryNode *node) {
	if (NULL == node) {
		return kErrorCode_Node;
	}

	MEMNODE_reset(node);
	node->ops_ = NULL;

	MM->free(node);

	return kErrorCode_Ok;
}

s16 MEMNODE_softFree (MemoryNode *node) {
	
	if (NULL == node)
		return kErrorCode_Node;

	MM->free(node);
	return kErrorCode_Ok;
}

s16 MEMNODE_setNext(MemoryNode *node, MemoryNode **next) {
	if (NULL == node)
		return kErrorCode_Node;
	if (NULL == *next)
		return kErrorCode_Node;

	node->next_ = (*next);
	return kErrorCode_Ok;
}

s16 MEMNODE_setPrev(MemoryNode *node, MemoryNode **prev) {
	
	if (NULL == node)
		return kErrorCode_Node;
	if (NULL == *prev)
		return kErrorCode_Node;

	node->prev_ = (*prev);
	return kErrorCode_Ok;

}

MemoryNode* MEMNODE_next(MemoryNode *node) {
	if (NULL == node)
		return NULL;
	return node->next_;
}

MemoryNode* MEMNODE_prev(MemoryNode *node) {
	if (NULL == node)
		return NULL;
	return node->prev_;
}

s16	MEMNODE_memSet(MemoryNode *node, u8 value) {
	if (NULL != node) {
		
		if (NULL != node->data_) {
			memset(node->data_, value, node->size_);
			return kErrorCode_Ok;
		}
			
		else
			return kErrorCode_Data;
	}
	else {
		return kErrorCode_Node;
	}
}
s16	MEMNODE_memCopy(MemoryNode *node, void *src, u16 bytes) {

	if (NULL == node) {
		return kErrorCode_Node;
	}
	
	if (node->data_ == src) {
		return kErrorCode_Ok;
	}

	if (NULL == src) {
		return kErrorCode_Source;
	}

	if (0 == bytes) {
		return kErrorCode_Size;
	}

	void* aux_data = MM->malloc(bytes);
	
	if (NULL == aux_data) {
		return kErrorCode_Malloc;
	}
		
	if (NULL != node->data_) {
		MM->free(node->data_);
		node->data_ = NULL;
	}


	memcpy(aux_data, src, bytes);
	node->data_ = aux_data;
	node->size_ = bytes;

	return kErrorCode_Ok;

}
s16 MEMNODE_memConcat(MemoryNode *node, void *src, u16 bytes) {
	   
	if (NULL == node) {
		return kErrorCode_Node;
	}

	if (NULL == src && NULL == node->data_){
		return kErrorCode_Memory;
	}

	if (NULL == src || 0 == bytes) {
		return kErrorCode_Ok;
	}

	if (NULL == node->data_) {
		MEMNODE_memCopy(node, src, bytes);
	}

		
	u8 *auxdata = MM->malloc(node->size_ + bytes);
	//auxdata2 = ()auxdata;
		
	memcpy(auxdata, node->data_, node->size_);
	//auxdata += node->size_;
		
	memcpy(auxdata + node->size_ , src, bytes);

	MM->free(node->data_);
	
	node->data_ = (void*)auxdata;
	node->size_ += bytes;
		
	return kErrorCode_Ok;
	
}
s16 MEMNODE_memMask(MemoryNode *node, u8 mask){
	if (NULL != node) {
		u8 *aux_data = (u8*)node->data_;
		for (int i = 0; i < node->size_; ++i) {
			aux_data[i] &= mask;
		}
	
		return kErrorCode_Ok;
	}
	else {
		return kErrorCode_Memory;
	}
}

void MEMNODE_print(MemoryNode *node) {
	 
	if (NULL != node) {
		if (NULL != node->data_) {
			
			unsigned char *p = node->data_;
			int i;
			for (i = 0; i < node->size_; i++) {
				printf("%c", p[i]);
			}
			printf("\n");
	
		}
		else
			printf("Null data");
	}
	else {
		printf("Null node");
	}
}