// adt_memory_node.c : 
// Toni Barella
// Algoritmos & Inteligencia Artificial
// ESAT 2016-2019
//
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "common_def.h"
#include "adt_vector.h"
#include "ABGS_MemoryManager\abgs_memory_manager.h"

static s16 VECTOR_destroy(Vector **vector);
static s16 VECTOR_reset(Vector *vector);
static s16 VECTOR_resize(Vector *vector, u16 new_size);
static u16 VECTOR_capacity(Vector *vector);
static u16 VECTOR_length(Vector *vector);
static bool VECTOR_isEmpty(Vector *vector);
static bool VECTOR_isFull(Vector *vector);
static void* VECTOR_first(Vector *vector);
static void* VECTOR_last(Vector *vector);
static void* VECTOR_at(Vector *vector, u16 position);
static s16 VECTOR_insertFirst(Vector *vector, void *data, u16 bytes);
static s16 VECTOR_insertLast(Vector *vector, void *data, u16 bytes);
static s16 VECTOR_insertAt(Vector *vector, void *data, u16 position, u16 bytes);
static void* VECTOR_extractFirst(Vector *vector);
static void* VECTOR_extractLast(Vector *vector);
static void* VECTOR_extractAt(Vector *vector, u16 position);
static s16 VECTOR_concat(Vector *vector, Vector *vector_src);
static s16 VECTOR_traverse(Vector *vector, void(*callback) (MemoryNode *));
static void VECTOR_print(Vector *vector);

struct vector_ops_s vector_ops =
{
	.destroy = VECTOR_destroy,	// Destroys the vector and its data
	.reset = VECTOR_reset,		// Resets vector's data
	.resize = VECTOR_resize, // Resizes the capacity of the vector. Some elements can be lost

	// State queries
	.capacity = VECTOR_capacity,		// returns the maximum number of elemets to store
	.length = VECTOR_length,			// current number of elements (<= capacity)
	.isEmpty = VECTOR_isEmpty,
	.isFull = VECTOR_isFull,

	// Data queries
	.first = VECTOR_first, // Returns a reference to the first element of the vector
	.last = VECTOR_last, // Returns a reference to the last element of the vector
	.at = VECTOR_at, // Returns a reference to the element oat a given position

	// Insertion
	.insertFirst = VECTOR_insertFirst, // Inserts an element in the first position of the vector
	.insertLast = VECTOR_insertLast, // Inserts an element in the last position of the vector
	.insertAt = VECTOR_insertAt, // Inserts an element at the given position of the vector
	
	// Extraction
	.extractFirst = VECTOR_extractFirst, // Extracts the first element of the vector
	.extractLast = VECTOR_extractLast, // Extracts the last element of the vector
	.extractAt = VECTOR_extractAt, // Extracts the element of the vector at the given position
	
	// Miscellaneous
	.concat = VECTOR_concat, // Concatenates two vectors
	.traverse = VECTOR_traverse, // Calls to a function from all elements of the vector
	.print = VECTOR_print // Print the features and content of the vector
	
};

Vector* VECTOR_create(u16 capacity) {
	
	if (0 >= capacity)
		return NULL;

	Vector *vector = MM->malloc(sizeof(Vector));
	//Vector *vector = (Vector*)malloc();
	vector->storage_ = MM->malloc(sizeof(MemoryNode)*capacity);
	vector->capacity_ = capacity;
	vector->ops_ = &vector_ops;

	vector->head_ = 0;
	vector->tail_ = 0;

	for (int i = 0; i < capacity; ++i) {
		MEMNODE_initWithoutCheck(&vector->storage_[i]);
	}

	return vector;

}

s16 VECTOR_destroy(Vector **vector) {
	
	if (NULL == (*vector))
		return kErrorCode_Vector;
	
	(*vector)->ops_->reset((*vector));
	
	MM->free((*vector)->storage_);
	(*vector)->ops_ = NULL;
	(*vector)->storage_ = NULL;
	(*vector)->capacity_ = 0;
	MM->free(*vector);	
	*vector = NULL;

	return kErrorCode_Ok;
}

s16 VECTOR_reset(Vector *vector) {

	if (NULL == vector)
		return kErrorCode_Vector;

	vector->head_ = 0;
	vector->tail_ = 0;
	
	for (int i = 0; i < vector->capacity_; ++i) {
		vector->storage_[i].ops_->reset(&vector->storage_[i]);
	}

	return kErrorCode_Ok;

}

s16 VECTOR_resize(Vector *vector, u16 new_size) {

	

	if (NULL == vector)
		return kErrorCode_Vector;

	if (0 >= new_size)
		return kErrorCode_Size;

	
	MemoryNode *aux_storage = MM->malloc(sizeof(MemoryNode)*new_size);
	
	for (int i = 0; i < new_size; ++i) {
		MEMNODE_initWithoutCheck(&aux_storage[i]);
	}
	
	u16 aux_size;

	if ((vector->tail_ - vector->head_) <= new_size) {
		aux_size = vector->tail_ - vector->head_;
	}
	else {
		aux_size = new_size;
		for (int i = vector->head_ + new_size; i < vector->tail_; i++) {
			aux_storage[0].ops_->reset(&vector->storage_[i]);
		}
	}

	memcpy(&aux_storage[0], &vector->storage_[vector->head_], sizeof(MemoryNode)*aux_size);	

	//memset(&vector->storage_[vector->head_], 0, sizeof(MemoryNode) * vector->capacity_);


	MM->free(vector->storage_);
	//aux_vec->ops_->destroy(vector);
	vector->storage_ = aux_storage;
	vector->capacity_ = new_size;
	vector->head_ = 0;
	vector->tail_ = aux_size;

	return kErrorCode_Ok;
}

//State queries

u16 VECTOR_capacity(Vector *vector) {
	if (NULL == vector) {
		return 0;
	}
	return vector->capacity_;
}

u16 VECTOR_length(Vector *vector) {
	if (NULL == vector) {
		return 0;
	}
	return vector->tail_ - vector->head_ ;
}

bool VECTOR_isEmpty(Vector *vector) {
	if (NULL == vector)
		return false;
	else if ((vector->tail_ - vector->head_) == 0) 
		return true;
	else 
		return false;

}

bool VECTOR_isFull(Vector *vector) {
	if (NULL == vector)
		return true;
	else
		return (vector->tail_ - vector->head_) == vector->capacity_;
}

//Data queries

void* VECTOR_first(Vector *vector) {
	if (NULL == vector) {
		printf("Null Vector\n");
		return NULL;
	}
	if (vector->ops_->isEmpty(vector)) {
		printf("Empty Vector\n");
		return NULL;
	}
	return vector->storage_[vector->head_].ops_->data(&vector->storage_[vector->head_]);
}

void* VECTOR_last(Vector *vector) {
	if (NULL == vector) {
		printf("Null Vector\n");
		return NULL;
	}
	if (vector->ops_->isEmpty(vector)) {
		printf("Empty Vector\n");
		return NULL;
	}
	return vector->storage_[vector->tail_ - 1].ops_->data(&vector->storage_[vector->tail_ - 1]);
}

void* VECTOR_at(Vector *vector, u16 position) {
	if (NULL == vector) {
		printf("Null Vector\n");
		return NULL;
	}		
	if (vector->ops_->isEmpty(vector)) {
		printf("Empty Vector\n");
		return NULL;
	}		
	if (0 > position || vector->capacity_ <= position)
		return NULL;
	return vector->storage_[position].ops_->data(&vector->storage_[vector->head_]);
}

//Insertion

s16 VECTOR_insertFirst(Vector *vector, void *data, u16 bytes) {


	if (NULL == vector)
		return kErrorCode_Vector;

	if (NULL == data)
		return kErrorCode_Data;

	if (0 == bytes)
		return kErrorCode_Size;

	if (vector->ops_->isFull(vector))
		return kErrorCode_Full;
	
	
	if (0 < vector->head_) {
		vector->storage_[vector->head_ - 1].ops_->setData(&vector->storage_[vector->head_ - 1], data, bytes);
		vector->head_--;
		return kErrorCode_Ok;
	}
	else {
		for (int i = vector->tail_ ; i > vector->head_ ; --i) {
			
			vector->storage_[i].ops_->setData(&vector->storage_[i], 
				vector->storage_[i - 1].ops_->data(&vector->storage_[i - 1]),
				vector->storage_[i - 1].ops_->size(&vector->storage_[i - 1]));
			vector->storage_[i - 1].ops_->softReset(&vector->storage_[i - 1]);
			
		}
		vector->storage_[vector->head_].ops_->setData(&vector->storage_[vector->head_], data, bytes);
		vector->tail_++;
		return kErrorCode_Ok;
	}
	

}

s16 VECTOR_insertLast(Vector *vector, void *data, u16 bytes) {

	if (NULL == vector)
		return kErrorCode_Vector;

	if (NULL == data)
		return kErrorCode_Data;

	if (0 == bytes)
		return kErrorCode_Size;

	if (vector->ops_->isFull(vector))
		return kErrorCode_Full;

	if (vector->capacity_ > vector->tail_) {
		vector->storage_[vector->tail_].ops_->setData(&vector->storage_[vector->tail_], data, bytes);
		vector->tail_++;
	}
	else if (0 < vector->head_) {
		for (int i = vector->head_; i < vector->tail_ ; ++i) {
			vector->storage_[i - 1].ops_->setData(&vector->storage_[i - 1],
				vector->storage_[i].ops_->data(&vector->storage_[i]),
				vector->storage_[i].ops_->size(&vector->storage_[i]));
			vector->storage_[i].ops_->softReset(&vector->storage_[i]);
		}
		vector->storage_[vector->tail_ - 1].ops_->setData(&vector->storage_[vector->tail_ - 1], data, bytes);
		vector->head_--;
	}
	
	return kErrorCode_Ok;
}

s16 VECTOR_insertAt(Vector *vector, void *data, u16 position, u16 bytes) {

	if (NULL == vector) 
		return kErrorCode_Vector;
		
	if (NULL == data) 
		return kErrorCode_Data;
	
	if (0 == bytes) 
		return kErrorCode_Size;

	if (vector->ops_->isFull(vector))
		return kErrorCode_Full;
		
	if (0 > position || vector->capacity_ <= position)
		return kErrorCode_Position;

	if (vector->ops_->isEmpty(vector)) {
		vector->storage_[vector->head_].ops_->setData(&vector->storage_[vector->head_], data, bytes);
		vector->tail_++;
		return kErrorCode_Ok;
	}


	
	if (position <= (vector->head_ - 1)) {
		vector->storage_[vector->head_ - 1].ops_->setData(&vector->storage_[vector->head_ - 1], data, bytes);
		vector->tail_++;
		return kErrorCode_Ok;
	}

	if (position >= (vector->tail_)) {
		vector->storage_[vector->tail_].ops_->setData(&vector->storage_[vector->tail_], data, bytes);
		vector->tail_++;
		return kErrorCode_Ok;
	}

	if (vector->capacity_ > vector->tail_) {
		for (int i = vector->tail_; i > position ; --i) {
			vector->storage_[i].ops_->setData(&vector->storage_[i],
				vector->storage_[i - 1].ops_->data(&vector->storage_[i - 1]),
				vector->storage_[i - 1].ops_->size(&vector->storage_[i - 1]));
			vector->storage_[i - 1].ops_->softReset(&vector->storage_[i - 1]);
		}
		
		vector->storage_[position].ops_->setData(&vector->storage_[position], data, bytes);
		vector->tail_++;
	}
	else if (0 < vector->head_) {
		for (int i = vector->head_; i <= position; ++i) {
			vector->storage_[i - 1].ops_->setData(&vector->storage_[i - 1],
				vector->storage_[i].ops_->data(&vector->storage_[i]),
				vector->storage_[i].ops_->size(&vector->storage_[i]));
			vector->storage_[i].ops_->softReset(&vector->storage_[i]);
		}
		vector->storage_[position].ops_->setData(&vector->storage_[position], data, bytes);
		vector->head_--;
	}
	return kErrorCode_Ok;

}

void* VECTOR_extractFirst(Vector *vector){

	if (NULL == vector)
		return NULL;
	else {
		void *data = vector->storage_[vector->head_].ops_->data(&vector->storage_[vector->head_]);

		vector->storage_[vector->head_].ops_->softReset(&vector->storage_[vector->head_]);

		++vector->head_;

		return data;
	}

}


void* VECTOR_extractLast(Vector *vector) {
	
	if (NULL == vector)
		return NULL;
	else {

		void *data = vector->storage_[vector->tail_ - 1].ops_->data(&vector->storage_[vector->tail_ - 1]);

		vector->storage_[vector->tail_ - 1].ops_->softReset(&vector->storage_[vector->tail_ - 1]);

		--vector->tail_;

		return data;
	}
}

void* VECTOR_extractAt(Vector *vector, u16 position){
	
	
	if (NULL == vector)
		return NULL;
	else if (0 > position || vector->capacity_ <= position)
		return NULL;
	else {

		void *data = vector->storage_[position].ops_->data(&vector->storage_[position]);
		vector->storage_[position].ops_->softReset(&vector->storage_[position]);

		for (int i = position; i < vector->tail_ - 1; ++i) {
			vector->storage_[i].ops_->setData(&vector->storage_[i],
				vector->storage_[i + 1].ops_->data(&vector->storage_[i + 1]),
				vector->storage_[i + 1].ops_->size(&vector->storage_[i + 1]));
			vector->storage_[i + 1].ops_->softReset(&vector->storage_[i + 1]);
		}

		--vector->tail_;

		return data;
	}

}

s16 VECTOR_concat(Vector *vector, Vector *vector_src) {

	if (NULL == vector || NULL == vector_src)
		return kErrorCode_Vector;
	
	vector->ops_->resize(vector, vector->capacity_ + vector_src->capacity_);
	for (int i = 0; i < vector_src->ops_->length(vector_src); ++i) {
		vector->storage_[vector->tail_ + i].ops_->memCopy(&vector->storage_[vector->tail_ + i],
			vector_src->storage_[vector_src->head_ + i].ops_->data(&vector_src->storage_[vector_src->head_ + i]),
			vector_src->storage_[vector_src->head_ + i].ops_->size(&vector_src->storage_[vector_src->head_ + i]));	
	}

	vector->tail_ += vector_src->ops_->length(vector_src);
	
	return kErrorCode_Ok;

}

s16 VECTOR_traverse(Vector *vector, void(*callback) (MemoryNode *)) {

	if (NULL == vector)
		return kErrorCode_Vector;

	for (int i = vector->head_; i < vector->tail_; ++i) {
		callback(&vector->storage_[i]);
	}

	return kErrorCode_Ok;
}

void VECTOR_print(Vector *vector) {

	if (NULL == vector)
		printf("\n NULL vector \n");
	else if (vector->ops_->isEmpty(vector))
		printf("\n Empty vector \n");
	else {
		printf("\n--------------------\n");
		printf("\nCapacity: %d\n", vector->capacity_);
		printf("Head: %d\n", vector->head_);
		printf("Tail: %d\n", vector->tail_);
		printf("Length: %d\n\n", vector->ops_->length(vector));


		for (int i = vector->head_; i < vector->tail_; ++i) {
			printf("Node %d: ", i);
			u8 *aux_data = (u8*)vector->storage_[i].ops_->data(&vector->storage_[i]);
			for (int j = 0; j < vector->storage_[i].ops_->size(&vector->storage_[i]); ++j) {
				printf("%c", *(aux_data + j));
			}
			printf("\n");
		}

		printf("\n--------------------\n");
	}
	
	

}