#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "adt_queue.h"
#include "common_def.h"
#include "ABGS_MemoryManager\abgs_memory_manager.h"

static s16 QUEUE_destroy(Queue **queue);
static s16 QUEUE_reset(Queue *queue);
static s16 QUEUE_resize(Queue *queue, u16 new_size);
static u16 QUEUE_capacity(Queue *queue);
static u16 QUEUE_length(Queue *queue);
static bool QUEUE_isEmpty(Queue *queue);
static bool QUEUE_isFull(Queue *queue);
static void* QUEUE_front(Queue *queue);
static void* QUEUE_back(Queue *queue);
static s16 QUEUE_enqueue(Queue *queue, void *data, u16 bytes);
static void* QUEUE_dequeue(Queue *queue);
static s16 QUEUE_concat(Queue *queue, Queue *queue_src);
static s16 QUEUE_traverse(Queue *queue, void(*callback) (MemoryNode *));
static void QUEUE_print(Queue *queue);

struct queue_ops_s queue_ops =
{
	.destroy = QUEUE_destroy,	// Destroys the vector and its data
	.reset = QUEUE_reset,		// Resets vector's data
	.resize = QUEUE_resize, // Resizes the capacity of the vector. Some elements can be lost

	// State queries
	.capacity = QUEUE_capacity,		// returns the maximum number of elemets to store
	.length = QUEUE_length,			// current number of elements (<= capacity)
	.isEmpty = QUEUE_isEmpty,
	.isFull = QUEUE_isFull,

	// Data queries
	.front = QUEUE_front, // Returns a reference to the first element of the vector
	.back = QUEUE_back, // Returns a reference to the last element of the vector

	// Insertion
	.enqueue = QUEUE_enqueue, // Inserts an element in the last position of the vector

	// Extraction
	.dequeue = QUEUE_dequeue, // Extracts the first element of the vector

	// Miscellaneous
	.concat = QUEUE_concat, // Concatenates two vectors
	.traverse = QUEUE_traverse, // Calls to a function from all elements of the vector
	.print = QUEUE_print // Print the features and content of the vector

};


Queue* QUEUE_create(u16 capacity) {
	Queue *aux_queue = MM->malloc(sizeof(Queue));
	aux_queue->storage_ = LIST_create(0);
	aux_queue->ops_ = &queue_ops;
	return aux_queue;
}

s16 QUEUE_destroy(Queue **queue)
{
	if (NULL == (*queue))
		return kErrorCode_Queue;
	(*queue)->storage_->ops_->destroy(&(*queue)->storage_);
	(*queue)->ops_ = NULL;
	MM->free((*queue));
	*queue = NULL;
	return kErrorCode_Ok;
}

s16 QUEUE_reset(Queue *queue) {
	if (NULL == queue)
		return kErrorCode_Queue;
	return queue->storage_->ops_->reset(queue->storage_);
}

s16 QUEUE_resize(Queue *queue, u16 new_size) {
	if (NULL == queue)
		return kErrorCode_Queue;
	return queue->storage_->ops_->resize(queue->storage_, new_size);
}

u16 QUEUE_capacity(Queue *queue) {
	return 0;
}

u16 QUEUE_length(Queue *queue) {
	if (NULL == queue)
		return kErrorCode_Queue;
	return queue->storage_->ops_->length(queue->storage_);
}

bool QUEUE_isEmpty(Queue *queue) {
	if (NULL == queue)
		return false;
	return queue->storage_->ops_->isEmpty(queue->storage_);
}

bool QUEUE_isFull(Queue *queue) {
	if (NULL == queue)
		return true;
	return queue->storage_->ops_->isFull(queue->storage_);
}

void* QUEUE_front(Queue *queue) {
	if (NULL == queue)
		return NULL;
	return queue->storage_->ops_->first(queue->storage_);
}

void* QUEUE_back(Queue *queue) {
	if (NULL == queue)
		return NULL;
	return queue->storage_->ops_->last(queue->storage_);
}

s16 QUEUE_enqueue(Queue *queue, void *data, u16 bytes) {
	if (NULL == queue)
		return kErrorCode_Queue;
	return queue->storage_->ops_->insertLast(queue->storage_,data,bytes);
}

void* QUEUE_dequeue(Queue *queue) {
	if (NULL == queue)
		return NULL;
	return queue->storage_->ops_->extractFirst(queue->storage_);
}

s16 QUEUE_concat(Queue *queue, Queue *queue_src) {
	if (NULL == queue)
		return kErrorCode_Queue;
	if (NULL == queue_src)
		return kErrorCode_Queue;

	return queue->storage_->ops_->concat(queue->storage_, queue_src->storage_);

}

s16 QUEUE_traverse(Queue *queue, void(*callback) (MemoryNode *)) {
	if (NULL == queue)
		return kErrorCode_Queue;
}

void QUEUE_print(Queue *queue) {
	if (NULL == queue)
		printf("\n NULL Queue \n");
	else
		queue->storage_->ops_->print(queue->storage_);
}
