#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "common_def.h"
#include "adt_stack.h"
#include "ABGS_MemoryManager/abgs_memory_manager.h"


static s16 STACK_destroy(Stack **stack);
static s16 STACK_reset(Stack *stack);
static s16 STACK_resize(Stack *stack, u16 new_size);
static u16 STACK_capacity(Stack *stack);
static u16 STACK_length(Stack *stack);
static bool STACK_isEmpty(Stack *stack);
static bool STACK_isFull(Stack *stack);
static void* STACK_top(Stack *stack);
static s16 STACK_push(Stack *stack, void *data, u16 bytes);
static void* STACK_pop(Stack *stack);
static s16 STACK_concat(Stack *stack, Stack *vector_src);
static s16 STACK_traverse(Stack *stack, void(*callback) (MemoryNode *));
static void STACK_print(Stack *stack);

// Memory Node's API Definitions
struct stack_ops_s stack_ops =
{
	.destroy = STACK_destroy,	// Destroys the vector and its data
	.reset = STACK_reset,		// Resets vector's data
	.resize = STACK_resize, // Resizes the capacity of the vector. Some elements can be lost

	// State queries
	.capacity = STACK_capacity,		// returns the maximum number of elemets to store
	.length = STACK_length,			// current number of elements (<= capacity)
	.isEmpty = STACK_isEmpty,
	.isFull = STACK_isFull,

	// Data queries
	.top = STACK_top, // Returns a reference to the last element of the vector

	// Insertion
	.push = STACK_push, // Inserts an element in the last position of the vector

	// Extraction
	.pop = STACK_pop, // Extracts the last element of the vector

	// Miscellaneous
	.concat = STACK_concat, // Concatenates two vectors
	.print = STACK_print // Print the features and content of the vector
};

Stack* STACK_create(u16 capacity) {
	if (0 >= capacity)
		return NULL;
	Stack *aux_stack = MM->malloc(sizeof(Stack));
	aux_stack->storage_ = VECTOR_create(capacity);
	aux_stack->ops_ = &stack_ops;
	return aux_stack;
}


s16 STACK_destroy(Stack **stack){
	if (NULL == (*stack))
		return kErrorCode_Stack;
	(*stack)->storage_->ops_->destroy(&((*stack)->storage_));
	(*stack)->ops_ = NULL;
	MM->free((*stack));
	*stack = NULL;
	return kErrorCode_Ok;
}

s16 STACK_reset(Stack *stack) {
	if (NULL == stack)
		return kErrorCode_Stack;
	return stack->storage_->ops_->reset(stack->storage_);
	
}

s16 STACK_resize(Stack *stack, u16 new_size) {
	if (NULL == stack)
		return kErrorCode_Stack;
	return stack->storage_->ops_->resize(stack->storage_, new_size);
}
u16 STACK_capacity(Stack *stack) {
	if (NULL == stack)
		return kErrorCode_Stack;
	return stack->storage_->ops_->capacity(stack->storage_);
}
u16 STACK_length(Stack *stack) {
	if (NULL == stack)
		return kErrorCode_Stack;
	return stack->storage_->ops_->length(stack->storage_);
}
bool STACK_isEmpty(Stack *stack) {
	if (NULL == stack)
		return false;
	return stack->storage_->ops_->isEmpty(stack->storage_);
}
bool STACK_isFull(Stack *stack) {
	if (NULL == stack)
		return true;
	return stack->storage_->ops_->isFull(stack->storage_);
}
void* STACK_top(Stack *stack) {
	if (NULL == stack)
		return NULL;
	return stack->storage_->ops_->last(stack->storage_);
}
s16 STACK_push(Stack *stack, void *data, u16 bytes) {
	if (NULL == stack)
		return kErrorCode_Stack;
	return stack->storage_->ops_->insertLast(stack->storage_,data,bytes);
}
void* STACK_pop(Stack *stack) {
	if (NULL == stack)
		return NULL;
	return stack->storage_->ops_->extractLast(stack->storage_);
}
s16 STACK_concat(Stack *stack, Stack *stack_src) {
	if (NULL == stack)
		return kErrorCode_Stack;
	if (NULL == stack_src)
		return kErrorCode_Stack;
	return stack->storage_->ops_->concat(stack->storage_,stack_src->storage_);
}
s16 STACK_traverse(Stack *stack, void(*callback) (MemoryNode *)) {

}
void STACK_print(Stack *stack) {
	if (NULL == stack)
		printf("\n NULL stack \n");
	else
		stack->storage_->ops_->print(stack->storage_);
}