#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "adt_list.h"
#include "common_def.h"
#include "ABGS_MemoryManager\abgs_memory_manager.h"

static s16 LIST_destroy(List **list);
static s16 LIST_reset(List *list);
static s16 LIST_resize(List *list, u16 new_size);
static u16 LIST_capacity(List *list);
static u16 LIST_length(List *list);
static bool LIST_isEmpty(List *list);
static bool LIST_isFull(List *list);
static void* LIST_first(List *list);
static void* LIST_last(List *list);
static void* LIST_at(List *list, u16 position);
static s16 LIST_insertFirst(List *list, void *data, u16 bytes);
static s16 LIST_insertLast(List *list, void *data, u16 bytes);
static s16 LIST_insertAt(List *list, void *data, u16 position, u16 bytes);
static s16 LIST_insertNodeLast(List *list, MemoryNode *node);
static void* LIST_extractFirst(List *list);
static void* LIST_extractLast(List *list);
static void* LIST_extractAt(List *list, u16 position);
static s16 LIST_concat(List *list, List *list_src);
static s16 LIST_traverse(List *list, void(*callback) (MemoryNode *));
static void LIST_print(List *list);

struct list_ops_s list_ops =
{
	.destroy = LIST_destroy,	// Destroys the vector and its data
	.reset = LIST_reset,		// Resets vector's data
	.resize = LIST_resize, // Resizes the capacity of the vector. Some elements can be lost

	// State queries
	.capacity = LIST_capacity,		// returns the maximum number of elemets to store
	.length = LIST_length,			// current number of elements (<= capacity)
	.isEmpty = LIST_isEmpty,
	.isFull = LIST_isFull,

	// Data queries
	.first = LIST_first, // Returns a reference to the first element of the vector
	.last = LIST_last, // Returns a reference to the last element of the vector
	.at = LIST_at, // Returns a reference to the element oat a given position

	// Insertion
	.insertFirst = LIST_insertFirst, // Inserts an element in the first position of the vector
	.insertLast = LIST_insertLast, // Inserts an element in the last position of the vector
	.insertAt = LIST_insertAt, // Inserts an element at the given position of the vector
	.insertNodeLast = LIST_insertNodeLast,

	// Extraction
	.extractFirst = LIST_extractFirst, // Extracts the first element of the vector
	.extractLast = LIST_extractLast, // Extracts the last element of the vector
	.extractAt = LIST_extractAt, // Extracts the element of the vector at the given position

	// Miscellaneous
	.concat = LIST_concat, // Concatenates two vectors
	.traverse = LIST_traverse, // Calls to a function from all elements of the vector
	.print = LIST_print // Print the features and content of the vector

};

List* LIST_create(u16 capacity) {
	
	List *aux_list = MM->malloc(sizeof(List));
	aux_list->capacity_ = 0;
	aux_list->first_ = NULL;
	aux_list->last_ = NULL;
	aux_list->length_ = 0;
	aux_list->ops_ = &list_ops;
	

	return aux_list;
}

s16 LIST_destroy(List **list) {
	
	if (NULL == *list)
		return kErrorCode_List;	
	
	(*list)->ops_->reset(*list);

	MM->free(*list);
	*list = NULL;

	return kErrorCode_Ok;
}

s16 LIST_reset(List *list) {
	
	if (NULL == list)
		return kErrorCode_List;	
	if (list->ops_->isEmpty(list))
		return kErrorCode_Ok;

	
	MemoryNode *aux_node;
	MemoryNode *next_node;
	

	aux_node = list->first_;
	u16 pos = 0;

	while (pos < list->length_) {
		next_node = aux_node->ops_->next(aux_node) ;
		aux_node->ops_->free(aux_node);		
		aux_node = next_node;
		pos++;
	}

	list->capacity_ = 0;
	list->first_ = NULL;
	list->last_ = NULL;
	list->length_ = 0;


}

s16 LIST_resize(List *list, u16 new_size) {
	
	if (NULL == list)
		return kErrorCode_List;
	
	return kErrorCode_Ok;
}

//State queries 

u16 LIST_capacity(List *list) {
	return 0;
}

u16 LIST_length(List *list) {
	if (NULL == list)
		return 0;
	return list->length_;
}

bool LIST_isEmpty(List *list) {
	if (NULL == list)
		return false;
	
	if (NULL == list->first_)
		return true;
	else
		return false;
}

bool LIST_isFull(List *list) {
	if (NULL == list)
		return true;
	else
		return false;
}

void* LIST_first(List *list) {
	if (NULL == list)
		return NULL;

	if (list->ops_->isEmpty(list))
		return NULL;

	return list->first_->ops_->data(list->first_);
}

void* LIST_last(List *list) {
	if (NULL == list)
		return NULL;

	if (list->ops_->isEmpty(list))
		return NULL;

	return list->last_->ops_->data(list->last_);
}

void* LIST_at(List *list, u16 position) {
	if (NULL == list)
		return NULL;

	if (list->ops_->isEmpty(list))
		return NULL;

	u16 pos = 0;
	
	MemoryNode *aux_node = list->first_;

	while (pos < position && aux_node) {
		aux_node = aux_node->ops_->next(aux_node);
		pos++;
	}

	if (NULL == aux_node)
		return NULL;

	return aux_node->ops_->data(aux_node);

}

//Insertion

s16 LIST_insertFirst(List *list, void *data, u16 bytes) {
	
	if (NULL == list)
		return kErrorCode_List;
	if (NULL == data)
		return kErrorCode_Data;
	if (0 >= bytes)
		return kErrorCode_Size;

	MemoryNode *aux_node = MM->malloc(sizeof(MemoryNode));
	
	MEMNODE_initWithoutCheck(aux_node);
	aux_node->ops_->setData(aux_node, data, bytes);

	if (list->ops_->isEmpty(list)) {
		list->first_ = aux_node;
		list->last_ = aux_node;
		++list->length_;
	}
	else {
		MemoryNode *aux_first = list->first_;
		list->first_ = aux_node;
		aux_node->ops_->setNext(aux_node, &aux_first);
		++list->length_;
	}

	return kErrorCode_Ok;

}

s16 LIST_insertLast(List *list, void *data, u16 bytes) {
	
	if (NULL == list)
		return kErrorCode_List;
	if (NULL == data)
		return kErrorCode_Data;
	if (0 >= bytes)
		return kErrorCode_Size;

	MemoryNode *aux_node = MM->malloc(sizeof(MemoryNode));

	MEMNODE_initWithoutCheck(aux_node);
	aux_node->ops_->setData(aux_node, data, bytes);

	if (list->ops_->isEmpty(list)) {
		list->first_ = aux_node;
		list->last_ = aux_node;
		++list->length_;
	}
	else {
		MemoryNode *aux_last = list->last_;
		list->last_ = aux_node;
		aux_last->ops_->setNext(aux_last, &aux_node);
		++list->length_;
	}

	return kErrorCode_Ok;

}

s16 LIST_insertAt(List *list, void *data, u16 position, u16 bytes) {
	
	if (NULL == list)
		return kErrorCode_List;
	if (NULL == data)
		return kErrorCode_Data;
	if (0 >= bytes)
		return kErrorCode_Size;

	if (0 == position)
		return list->ops_->insertFirst(list, data, bytes);

	if (list->length_ <= position)
		return list->ops_->insertLast(list, data, bytes);

	MemoryNode *aux_node = MM->malloc(sizeof(MemoryNode));

	MEMNODE_initWithoutCheck(aux_node);
	aux_node->ops_->setData(aux_node, data, bytes);

	if (list->ops_->isEmpty(list)) {
		list->first_ = aux_node;
		list->last_ = aux_node;
		++list->length_;
		return kErrorCode_Ok;
	}

	u16 pos = 0;

	MemoryNode *aux = list->first_;
	MemoryNode *prev;

	while (pos < position) {
		prev = aux;
		aux = aux->ops_->next(aux);
		pos++;
	}
	
	prev->ops_->setNext(prev, &aux_node);
	aux_node->ops_->setNext(aux_node, &aux);
	list->length_++;

	return kErrorCode_Ok;
	
}

s16 LIST_insertNodeLast(List *list, MemoryNode *node)
{
	if (NULL == list)
		return kErrorCode_List;
	if (NULL == node)
		return kErrorCode_Node;

	
	if (list->ops_->isEmpty(list)) {
		list->first_ = node;
		list->last_ = node;
		++list->length_;
	}
	else {
		MemoryNode *aux_last = list->last_;
		list->last_ = node;
		aux_last->ops_->setNext(aux_last, &node);
		++list->length_;
	}

	return kErrorCode_Ok;
}

void* LIST_extractFirst(List *list){
	
	if (NULL == list)
		return NULL;
	if (list->ops_->isEmpty(list))
		return NULL;

	if (1 == list->length_) {
		void *data = list->first_->ops_->data(list->first_);
		list->first_->ops_->softFree(list->first_);
		list->first_ = NULL;
		list->last_ = NULL;
		list->length_ = 0;
		return data;
	}
	
	MemoryNode *current = list->first_;
	MemoryNode *next = current->ops_->next(current);
	void *data = current->ops_->data(current);
	
	current->ops_->softFree(current);
	list->first_ = next;
	list->length_--;

	return data;

}

void* LIST_extractLast(List *list) {
	
	if (NULL == list)
		return NULL;
	
	if (list->ops_->isEmpty(list))
		return NULL;

	if (1 == list->length_) {
		void *data = list->first_->ops_->data(list->first_);
		list->first_->ops_->softFree(list->first_);
		list->first_ = NULL;
		list->last_ = NULL;
		list->length_ = 0;
		return data;
	} 

	MemoryNode *current = list->first_;
	MemoryNode *prev = NULL;

	u16 pos = 0;

	while (pos < (list->ops_->length(list)-1)) {
		prev = current;
		current = current->ops_->next(current);
		pos++;
	}

	void *data = current->ops_->data(current);

	current->ops_->softFree(current);
	list->last_ = prev;
	list->length_--;
	return data;
}

void* LIST_extractAt(List *list, u16 position) {
	
	if (NULL == list)
		return NULL; 
	
	if ( position >= (list->length_ - 1)) {
		return list->ops_->extractLast(list);
	}

	if (0 == position)
		return list->ops_->extractFirst(list);

	if (1 == list->length_) {
		void *data = list->first_->ops_->data(list->first_);		
		list->first_->ops_->softFree(list->first_);
		list->first_ = NULL;
		list->last_ = NULL;
		list->length_ = 0;
		return data;
	}
	
	MemoryNode *current = list->first_;
	MemoryNode *prev = NULL;
	

	u16 pos = 0;
	
	while (pos < position) {
		prev = current;
		current = current->ops_->next(current);
		++pos;
	}
	
	void *data = current->ops_->data(current);
	MemoryNode *aux = current->ops_->next(current);
	prev->ops_->setNext(prev, &aux);
	current->ops_->softFree(current);
	list->length_--;

	return data;
	
}

s16 LIST_concat(List *list, List *list_src) {
	
	if (NULL == list)
		return kErrorCode_List;
	if (NULL == list_src)
		return kErrorCode_List;

	u16 pos = 0;

	MemoryNode *src = list_src->first_;
	MemoryNode *dst = list->last_;

	while (pos < list_src->length_) {
		MemoryNode *aux_node = MEMNODE_create();
		aux_node->ops_->memCopy(aux_node, src->ops_->data(src), src->ops_->size(src));
		list->ops_->insertNodeLast(list, aux_node);

		src = src->ops_->next(src);
		pos++;
	}

	return kErrorCode_Ok;

}

s16 LIST_traverse(List *list, void(*callback) (MemoryNode *)) {
	
}

void LIST_print(List *list) {

	if (NULL == list)
		printf("\n NULL List \n");
	else if (list->ops_->isEmpty(list))
		printf("\n Empty List \n");
	else {

		MemoryNode *current = list->first_;
		u16 pos = 0;

		while (pos < list->length_) {
			printf("Node %d \n", pos);
			current->ops_->print(current);
			printf("\n");
			pos++;
			current = current->ops_->next(current);
		}
	}
}


