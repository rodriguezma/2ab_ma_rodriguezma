#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "adt_dllist.h"
#include "common_def.h"
#include "ABGS_MemoryManager\abgs_memory_manager.h"

static s16 DLLIST_destroy(DLList **list);
static s16 DLLIST_reset(DLList *list);
static s16 DLLIST_resize(DLList *list, u16 new_size);
static u16 DLLIST_capacity(DLList *list);
static u16 DLLIST_length(DLList *list);
static bool DLLIST_isEmpty(DLList *list);
static bool DLLIST_isFull(DLList *list);
static void* DLLIST_first(DLList *list);
static void* DLLIST_last(DLList *list);
static void* DLLIST_at(DLList *list, u16 position);
static s16 DLLIST_insertFirst(DLList *list, void *data, u16 bytes);
static s16 DLLIST_insertLast(DLList *list, void *data, u16 bytes);
static s16 DLLIST_insertAt(DLList *list, void *data, u16 position, u16 bytes);
static s16 DLLIST_insertNodeLast(DLList *list, MemoryNode *node);
static void* DLLIST_extractFirst(DLList *list);
static void* DLLIST_extractLast(DLList *list);
static void* DLLIST_extractAt(DLList *list, u16 position);
static s16 DLLIST_concat(DLList *list, DLList *list_src);
static s16 DLLIST_traverse(DLList *list, void(*callback) (MemoryNode *));
static void DLLIST_print(DLList *list);

struct dllist_ops_s dllist_ops =
{
	.destroy = DLLIST_destroy,	// Destroys the vector and its data
	.reset = DLLIST_reset,		// Resets vector's data
	.resize = DLLIST_resize, // Resizes the capacity of the vector. Some elements can be lost

	// State queries
	.capacity = DLLIST_capacity,		// returns the maximum number of elemets to store
	.length = DLLIST_length,			// current number of elements (<= capacity)
	.isEmpty = DLLIST_isEmpty,
	.isFull = DLLIST_isFull,

	// Data queries
	.first = DLLIST_first, // Returns a reference to the first element of the vector
	.last = DLLIST_last, // Returns a reference to the last element of the vector
	.at = DLLIST_at, // Returns a reference to the element oat a given position

	// Insertion
	.insertFirst = DLLIST_insertFirst, // Inserts an element in the first position of the vector
	.insertLast = DLLIST_insertLast, // Inserts an element in the last position of the vector
	.insertAt = DLLIST_insertAt, // Inserts an element at the given position of the vector
	.insertNodeLast = DLLIST_insertNodeLast,

	// Extraction
	.extractFirst = DLLIST_extractFirst, // Extracts the first element of the vector
	.extractLast = DLLIST_extractLast, // Extracts the last element of the vector
	.extractAt = DLLIST_extractAt, // Extracts the element of the vector at the given position

	// Miscellaneous
	.concat = DLLIST_concat, // Concatenates two vectors
	.traverse = DLLIST_traverse, // Calls to a function from all elements of the vector
	.print = DLLIST_print // Print the features and content of the vector

};


DLList* DLLIST_create(u16 capacity) {

	DLList *aux_list = MM->malloc(sizeof(DLList));
	aux_list->capacity_ = 0;
	aux_list->first_ = NULL;
	aux_list->last_ = NULL;
	aux_list->length_ = 0;
	aux_list->ops_ = &dllist_ops;


	return aux_list;
}

s16 DLLIST_destroy(DLList **list) {

	if (NULL == *list)
		return kErrorCode_List;

	(*list)->ops_->reset(*list);

	MM->free(*list);
	*list = NULL;

	return kErrorCode_Ok;
}

s16 DLLIST_reset(DLList *list) {

	if (NULL == list)
		return kErrorCode_List;
	if (list->ops_->isEmpty(list))
		return kErrorCode_Ok;


	MemoryNode *aux_node;
	MemoryNode *next_node;


	aux_node = list->first_;
	u16 pos = 0;

	while (pos < list->length_) {
		
		next_node = aux_node->ops_->next(aux_node);
		aux_node->ops_->free(aux_node);
		aux_node = next_node;
		pos++;
	}

	list->capacity_ = 0;
	list->first_ = NULL;
	list->last_ = NULL;
	list->length_ = 0;


}

s16 DLLIST_resize(DLList *list, u16 new_size) {

	if (NULL == list)
		return kErrorCode_List;

	return kErrorCode_Ok;
}

//State queries 

u16 DLLIST_capacity(DLList *list) {
	return 0;
}

u16 DLLIST_length(DLList *list) {
	if (NULL == list)
		return 0;
	return list->length_;
}

bool DLLIST_isEmpty(DLList *list) {
	if (NULL == list)
		return true;

	if (NULL == list->first_)
		return true;
	else
		return false;
}

bool DLLIST_isFull(DLList *list) {
	if (NULL == list)
		return false;
}

void* DLLIST_first(DLList *list) {
	if (NULL == list)
		return NULL;

	if (list->ops_->isEmpty(list))
		return NULL;

	return list->first_->ops_->data(list->first_);
}

void* DLLIST_last(DLList *list) {
	if (NULL == list)
		return NULL;

	if (list->ops_->isEmpty(list))
		return NULL;

	return list->last_->ops_->data(list->last_);
}

void* DLLIST_at(DLList *list, u16 position) {
	if (NULL == list)
		return NULL;

	if (list->ops_->isEmpty(list))
		return NULL;

	u16 pos = 0;

	MemoryNode *aux_node = list->first_;

	while (pos < position && aux_node) {
		
		aux_node = aux_node->ops_->next(aux_node);
		pos++;
	}

	if (NULL == aux_node)
		return NULL;

	return aux_node->ops_->data(aux_node);

}

//Insertion

s16 DLLIST_insertFirst(DLList *list, void *data, u16 bytes) {

	if (NULL == list)
		return kErrorCode_List;
	if (NULL == data)
		return kErrorCode_Data;
	if (0 >= bytes)
		return kErrorCode_Size;

	MemoryNode *aux_node = MM->malloc(sizeof(MemoryNode));

	MEMNODE_initWithoutCheck(aux_node);
	aux_node->ops_->setData(aux_node, data, bytes);

	if (list->ops_->isEmpty(list)) {
		list->first_ = aux_node;
		list->last_ = aux_node;
		++list->length_;
	}
	else {
		MemoryNode *aux_first = list->first_;
		list->first_ = aux_node;
		
		aux_node->ops_->setNext(aux_node, &aux_first);
		aux_first->ops_->setPrev(aux_first, &aux_node);
		++list->length_;
	}

	return kErrorCode_Ok;

}

s16 DLLIST_insertLast(DLList *list, void *data, u16 bytes) {

	if (NULL == list)
		return kErrorCode_List;
	if (NULL == data)
		return kErrorCode_Data;
	if (0 >= bytes)
		return kErrorCode_Size;

	MemoryNode *aux_node = MM->malloc(sizeof(MemoryNode));

	MEMNODE_initWithoutCheck(aux_node);
	aux_node->ops_->setData(aux_node, data, bytes);

	if (list->ops_->isEmpty(list)) {
		list->first_ = aux_node;
		list->last_ = aux_node;
		++list->length_;
	}
	else {
		MemoryNode *aux_last = list->last_;
		list->last_ = aux_node;
		
		aux_last->ops_->setNext(aux_last, &aux_node);
		aux_node->ops_->setPrev(aux_node, &aux_last);
		
		++list->length_;
	}

	return kErrorCode_Ok;

}

s16 DLLIST_insertAt(DLList *list, void *data, u16 position, u16 bytes) {

	if (NULL == list)
		return kErrorCode_List;
	if (NULL == data)
		return kErrorCode_Data;
	if (0 >= bytes)
		return kErrorCode_Size;

	if (0 == position)
		return list->ops_->insertFirst(list, data, bytes);

	if (list->length_ <= position)
		return list->ops_->insertLast(list, data, bytes);

	MemoryNode *aux_node = MM->malloc(sizeof(MemoryNode));

	MEMNODE_initWithoutCheck(aux_node);
	aux_node->ops_->setData(aux_node, data, bytes);

	if (list->ops_->isEmpty(list)) {
		list->first_ = aux_node;
		list->last_ = aux_node;
		++list->length_;
		return kErrorCode_Ok;
	}

	u16 pos = 0;

	MemoryNode *aux = list->first_;


	while (pos < position) {
	
		aux = aux->ops_->next(aux);
		pos++;
	}

	MemoryNode *prev = aux->ops_->prev(aux);
	MemoryNode *next = aux->ops_->next(aux);
	

	prev->ops_->setNext(prev, &aux_node);

	aux_node->ops_->setPrev(aux_node, &prev);
	

	aux_node->ops_->setNext(aux_node, &aux);

	aux->ops_->setPrev(aux, &aux_node);
	list->length_++;

	return kErrorCode_Ok;

}

s16 DLLIST_insertNodeLast(DLList *list, MemoryNode *node)
{
	if (NULL == list)
		return kErrorCode_List;
	if (NULL == node)
		return kErrorCode_Node;


	if (list->ops_->isEmpty(list)) {
		list->first_ = node;
		list->last_ = node;
		++list->length_;
	}
	else {
		MemoryNode *aux_last = list->last_;
		list->last_ = node;

		aux_last->ops_->setNext(aux_last, &node);

		node->ops_->setPrev(node, &aux_last);
		++list->length_;
	}

	return kErrorCode_Ok;
}

void* DLLIST_extractFirst(DLList *list) {

	if (NULL == list)
		return NULL;
	if (list->ops_->isEmpty(list))
		return NULL;

	if (1 == list->length_) {
		void *data = list->first_->ops_->data(list->first_);
		list->first_->ops_->softFree(list->first_);
		list->first_ = NULL;
		list->last_ = NULL;
		list->length_ = 0;
		return data;
	}

	MemoryNode *current = list->first_;

	MemoryNode *next = current->ops_->next(current);
	void *data = current->ops_->data(current);

	current->ops_->softFree(current);
	list->first_ = next;

	list->length_--;

	return data;

}

void* DLLIST_extractLast(DLList *list) {

	if (NULL == list)
		return NULL;

	if (list->ops_->isEmpty(list))
		return NULL;

	if (1 == list->length_) {
		void *data = list->first_->ops_->data(list->first_);
		list->first_->ops_->softFree(list->first_);
		list->first_ = NULL;
		list->last_ = NULL;
		list->length_ = 0;
		return data;
	}

	MemoryNode *current = list->last_;

	MemoryNode *prev = current->ops_->prev(current);


	void *data = current->ops_->data(current);

	current->ops_->softFree(current);
	list->last_ = prev;

	list->length_--;

	return data;

}

void* DLLIST_extractAt(DLList *list, u16 position) {

	if (NULL == list)
		return NULL;

	if (position >= (list->length_ - 1)) {
		return list->ops_->extractLast(list);
	}

	if (0 == position)
		return list->ops_->extractFirst(list);

	if (1 == list->length_) {
		void *data = list->first_->ops_->data(list->first_);
		list->first_->ops_->softFree(list->first_);
		list->first_ = NULL;
		list->last_ = NULL;
		list->length_ = 0;
		return data;
	}

	MemoryNode *current = list->first_;
	MemoryNode *prev;
	MemoryNode *next;


	u16 pos = 0;

	while (pos < position) {

		current = current->ops_->next(current);
		++pos;
	}

	void *data = current->ops_->data(current);

	prev = current->ops_->prev(current);

	next = current->ops_->next(current);

	prev->ops_->setNext(prev, &next);

	next->ops_->setPrev(next, &prev);
	current->ops_->softFree(current);
	list->length_--;

	return data;

}

s16 DLLIST_concat(DLList *list, DLList *list_src) {

	if (NULL == list)
		return kErrorCode_List;
	if (NULL == list_src)
		return kErrorCode_List;

	u16 pos = 0;

	MemoryNode *src = list_src->first_;
	MemoryNode *dst = list->last_;

	while (pos < list_src->length_) {
		MemoryNode *aux_node = MEMNODE_create();
		aux_node->ops_->memCopy(aux_node, src->ops_->data(src), src->ops_->size(src));
		list->ops_->insertNodeLast(list, aux_node);
		

		src = src->ops_->next(src);
		pos++;
	}

	return kErrorCode_Ok;

}

s16 DLLIST_traverse(DLList *list, void(*callback) (MemoryNode *)) {

}

void DLLIST_print(DLList *list) {

	if (NULL == list)
		printf("\n NULL List \n");
	else if (list->ops_->isEmpty(list))
		printf("\n Empty List \n");
	else {

		MemoryNode *current = list->first_;
		u16 pos = 0;

		while (pos < list->length_) {
			printf("Node %d \n", pos);
			current->ops_->print(current);
			printf("\n");
			pos++;

			current = current->ops_->next(current);
		}
	}
}