var annotated_dup =
[
    [ "adt_dllist_s", "structadt__dllist__s.html", "structadt__dllist__s" ],
    [ "adt_list_s", "structadt__list__s.html", "structadt__list__s" ],
    [ "adt_queue_s", "structadt__queue__s.html", "structadt__queue__s" ],
    [ "adt_stack_s", "structadt__stack__s.html", "structadt__stack__s" ],
    [ "adt_vector_s", "structadt__vector__s.html", "structadt__vector__s" ],
    [ "dllist_ops_s", "structdllist__ops__s.html", "structdllist__ops__s" ],
    [ "list_ops_s", "structlist__ops__s.html", "structlist__ops__s" ],
    [ "memory_node_ops_s", "structmemory__node__ops__s.html", "structmemory__node__ops__s" ],
    [ "memory_node_s", "structmemory__node__s.html", "structmemory__node__s" ],
    [ "queue_ops_s", "structqueue__ops__s.html", "structqueue__ops__s" ],
    [ "stack_ops_s", "structstack__ops__s.html", "structstack__ops__s" ],
    [ "vector_ops_s", "structvector__ops__s.html", "structvector__ops__s" ]
];