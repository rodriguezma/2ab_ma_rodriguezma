var searchData=
[
  ['enqueue',['enqueue',['../structqueue__ops__s.html#a009f714b3bc64013d8523c439259b43b',1,'queue_ops_s']]],
  ['extractat',['extractAt',['../structdllist__ops__s.html#af0cb7d2404c22bf3190ddef432f64f89',1,'dllist_ops_s::extractAt()'],['../structlist__ops__s.html#ab0b131eb8a13e8093c2cf378c90c0000',1,'list_ops_s::extractAt()'],['../structvector__ops__s.html#ae9a65728128edd1d8fe6653e4fb16bad',1,'vector_ops_s::extractAt()']]],
  ['extractfirst',['extractFirst',['../structdllist__ops__s.html#a7290c99eb29339e245687667a12752d1',1,'dllist_ops_s::extractFirst()'],['../structlist__ops__s.html#a8b065d7499f74aa6950e60c8d1c5c7a6',1,'list_ops_s::extractFirst()'],['../structvector__ops__s.html#af2b81db8a8c6de300a0c534b706c3147',1,'vector_ops_s::extractFirst()']]],
  ['extractlast',['extractLast',['../structdllist__ops__s.html#afb200d23cdcd09299a2ad43f1442efbf',1,'dllist_ops_s::extractLast()'],['../structlist__ops__s.html#a1083cecf65008a26c095f95412f16bb8',1,'list_ops_s::extractLast()'],['../structvector__ops__s.html#a894800a5e4120597f0329f8a972de02b',1,'vector_ops_s::extractLast()']]]
];
