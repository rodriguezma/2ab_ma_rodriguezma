var structqueue__ops__s =
[
    [ "back", "structqueue__ops__s.html#abb11c24405d4d4c1059ce13e8169d526", null ],
    [ "capacity", "structqueue__ops__s.html#a8441546ff954391b7579af72e7ac3594", null ],
    [ "concat", "structqueue__ops__s.html#a3278c25364958a09407fa22a4f724f34", null ],
    [ "dequeue", "structqueue__ops__s.html#aed76ef27d0fab8b775a0eef4a2d90118", null ],
    [ "destroy", "structqueue__ops__s.html#a100b9285cc8da0c7717f43a78fbef29a", null ],
    [ "enqueue", "structqueue__ops__s.html#a009f714b3bc64013d8523c439259b43b", null ],
    [ "front", "structqueue__ops__s.html#a6966fa64f0cd64b0fcb07368de3f2ff6", null ],
    [ "isEmpty", "structqueue__ops__s.html#aa030f18ef08b3b74d65e3efdaa5f0a08", null ],
    [ "isFull", "structqueue__ops__s.html#aa24e4b4e873eefd9d361c28b858a5c7e", null ],
    [ "length", "structqueue__ops__s.html#a0159fe2d0d14e0addc67efdcffd6a106", null ],
    [ "print", "structqueue__ops__s.html#abcd79c06159343ff815f243b0e3e74da", null ],
    [ "reset", "structqueue__ops__s.html#afa645fcb0943c862c44671cdfc35b27c", null ],
    [ "resize", "structqueue__ops__s.html#a2ccb3cd48b7d03e092b1e92466c8d12e", null ],
    [ "traverse", "structqueue__ops__s.html#a08d429c964f7d5d4ac33437322ef3158", null ]
];