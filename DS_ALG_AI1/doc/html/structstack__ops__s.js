var structstack__ops__s =
[
    [ "capacity", "structstack__ops__s.html#ac708d71669829eb33ade445672406ab9", null ],
    [ "concat", "structstack__ops__s.html#a2cee54588582288392013f7cec1ec7da", null ],
    [ "destroy", "structstack__ops__s.html#a7ec5d5aa6c9941319934f58a93f917b8", null ],
    [ "isEmpty", "structstack__ops__s.html#a9142e19f6661b8b781e489d8347a87de", null ],
    [ "isFull", "structstack__ops__s.html#a0e3c43bcf4b49dcec6fb055026de7158", null ],
    [ "length", "structstack__ops__s.html#ac2008f7e21f2e1524679bafe591d5ff3", null ],
    [ "pop", "structstack__ops__s.html#a441faec8a6dfb23b1a3bf1fe84e0924b", null ],
    [ "print", "structstack__ops__s.html#a9d70b84f517dd82a304da11b1807eb3c", null ],
    [ "push", "structstack__ops__s.html#a5c7035565e4131e6c78e37efe51152e4", null ],
    [ "reset", "structstack__ops__s.html#a60bf7c553718b628b6e666fb5974adcc", null ],
    [ "resize", "structstack__ops__s.html#a4042238d3561db716b568eabc1a99b72", null ],
    [ "top", "structstack__ops__s.html#a0cef49408995e4f185f28540c2053488", null ]
];