var structmemory__node__ops__s =
[
    [ "data", "structmemory__node__ops__s.html#a36adf5dc8abc5a2cffb136827b5bbb75", null ],
    [ "free", "structmemory__node__ops__s.html#ae1351951e1bca80e008bd6d32582dff5", null ],
    [ "memConcat", "structmemory__node__ops__s.html#aaec24f26495e5e53018f5fd686852fdb", null ],
    [ "memCopy", "structmemory__node__ops__s.html#a6d56057145412b1b6ebbba4006638b7b", null ],
    [ "memMask", "structmemory__node__ops__s.html#a817c65ad0b6c2bcc52e80d4dfef74399", null ],
    [ "memSet", "structmemory__node__ops__s.html#a06c05f2ccf5c23a232df135f2e7a2f52", null ],
    [ "next", "structmemory__node__ops__s.html#a8c2fd5987a65a1ec5050be547384ae66", null ],
    [ "prev", "structmemory__node__ops__s.html#ad672d5bf39cd34fdadcb59e009268762", null ],
    [ "print", "structmemory__node__ops__s.html#a785bfc1434d849c6b0dd77bc3cd4195e", null ],
    [ "reset", "structmemory__node__ops__s.html#a50b5474576ccd1add08efa49a64d65a4", null ],
    [ "setData", "structmemory__node__ops__s.html#aff8c997e02012501200e0794fbaf4aee", null ],
    [ "setNext", "structmemory__node__ops__s.html#ae7963ae23fac02a9ae0a3d2a032d56b7", null ],
    [ "setPrev", "structmemory__node__ops__s.html#a4ed4b8ff89f27c8501e5afb17032e45d", null ],
    [ "size", "structmemory__node__ops__s.html#ab71d9dc13a2a637550016d5bac6af7d3", null ],
    [ "softFree", "structmemory__node__ops__s.html#a37d12ea00c4c43785e2155f1cb5e941e", null ],
    [ "softReset", "structmemory__node__ops__s.html#a48510e6feaa85f50af3b833aa157c6c8", null ]
];