// common_def.h
// Toni Barella
// Algoritmos & Inteligencia Artificial
// ESAT 2016-2019
//
#ifndef __COMMON_DEF_H__
#define __COMMON_DEF_H__

#define VERBOSE_

typedef enum
{
	kErrorCode_Ok = 0,
	kErrorCode_Node = -1,
	kErrorCode_Data = -2,
	kErrorCode_Source = -3,
	kErrorCode_Size = -4,
	kErrorCode_Malloc = -5,
	kErrorCode_Memory = -10,
	kErrorCode_File = -20,
	kErrorCode_Full = -31,
	kErrorCode_Position = -32,
	kErrorCode_Vector = -30,
	kErrorCode_List = -40,
	kErrorCode_Stack = -50,
	kErrorCode_Queue = -60
} ErrorCode;

#endif // __COMMON_DEF_H__