#ifndef __ADT_STACK_H__
#define __ADT_STACK_H__

#include "adt_vector.h"

typedef struct adt_stack_s
{
	Vector *storage_;
	struct stack_ops_s *ops_;
} Stack;


struct stack_ops_s
{
	/// \brief Destroys the stack and its data
	/// \param Stack
	/// \return ErrorCode
	s16(*destroy) (Stack **stack);	

	/// \brief Resets the stack, clean the data
	/// \param Stack
	/// \return ErrorCode
	s16(*reset) (Stack *stack);		

	/// \brief Resizes the stack, can lose data
	/// \param Stack
	/// \param New size
	/// \return ErrorCode
	s16(*resize)(Stack *stack, u16 new_size); 

	/// \brief Returns the capacity of the stack
	/// \param Stack
	/// \return Capacity
	u16(*capacity)(Stack *stack);	


	/// \brief Returns the stack length, taken positions
	/// \param Stack
	/// \return Length
	u16(*length)(Stack *stack);		


	/// \brief Checks if the stack is empty
	/// \param Stack
	/// \return Boolean
	bool(*isEmpty) (Stack *stack);


	/// \brief Checks if the stack is full
	/// \param Stack
	/// \return Boolean
	bool(*isFull) (Stack *stack);

	/// \brief Returns a referenece to the data in the top of the stack
	/// \param Stack
	/// \return Data
	void* (*top)(Stack *stack); 
	
	/// \brief Insert a node in the top with the given data 
	/// \param Stack
	/// \param Data
	/// \param Size
	/// \return ErrorCode
	s16(*push) (Stack *stack, void *data, u16 bytes); 
	
	/// \brief Extracts the data at the top of the Stack, destroys the node
	/// \param Stack
	/// \return Data
	void*(*pop) (Stack *stack); 
	
	/// \brief Concats the the two stacks in the first one
	/// \param Destination Stack
	/// \param Source stack
	/// \return Error Code
	s16(*concat) (Stack *stack, Stack *stack_src); 

	/// \brief Prints the stacks information
	/// \param Stack
	void(*print)(Stack *stack); // Print the features and content of the stack
};

/// \brief Creates a stack with the given capacity and returns the reference
/// \param Capacity
/// \return Stack reference
Stack* STACK_create(u16 capacity); // Create a new stack

#endif //__ADT_STACK_H__