// adt_list.h

#ifndef __ADT_QUEUE_H__
#define __ADT_QUEUE_H__

#include "adt_list.h"

typedef struct adt_queue_s
{
	List *storage_;
	struct queue_ops_s *ops_;
} Queue;

struct queue_ops_s
{
	/// \brief Destroy the queue and set to NULL
	/// \param Queue 
	/// \return Error code
	s16(*destroy) (Queue **queue);	

	/// \brief Reset the queue, all the data is destroyed
	/// \param Queue
	/// \return Error code
	s16(*reset) (Queue *queue);		

	/// \brief Resize the queue. Irrelevant, queue without capacity
	/// \param Queue 
	/// \return Error code
	s16(*resize)(Queue *queue, u16 new_size); 

	/// \brief Capacity of the queue, allways infinite, irrelevant
	/// \param Queue 
	/// \return Capacity
	u16(*capacity)(Queue *queue);		

	/// \brief Lentgh of the queue, number of nodes currently inserted
	/// \param Queue 
	/// \return Length
	u16(*length)(Queue *queue);		

	/// \brief If the queue doesnt have nodes
	/// \param Queue
	/// \return Boolean
	bool(*isEmpty) (Queue *queue);

	/// \brief If the queue is full, never happends, irrelevant
	/// \param Queue
	/// \return Boolean
	bool(*isFull) (Queue *queue);

	/// \brief Retruns a reference to the data of the first node
	/// \param Queue
	/// \return Data
	void* (*front)(Queue *queue);

	/// \brief Retruns a reference to the data of the last node
	/// \param Queue
	/// \return Data
	void* (*back)(Queue *queue); 

	/// \brief Inserts a node in the last position, with the given data
	/// \param Queue
	/// \param Data
	/// \param Data size
	/// \return Error code
	s16(*enqueue) (Queue *queue, void *data, u16 bytes);  
	
	/// \brief Extracts the data of the first node, destroys the node
	/// \param Queue
	/// \return Data
	void*(*dequeue) (Queue *queue); 
	
	/// \brief Concatenates the two queues in the first one
	/// \param Destination queue
	/// \param Source queue
	/// \return Error code
	s16(*concat) (Queue *queue, Queue *queue_src); 

	/// \brief Calls to the given memory node function in each node
	/// \param Queue
	/// \param Function callback
	/// \return Error code
	s16(*traverse)(Queue *queue, void(*callback) (MemoryNode *));

	/// \brief Prints the queue information
	/// \param Queue
	void(*print)(Queue *queue);
};

/// \brief Creates a Queue, and returns the reference. Irrelevant capacity.
/// \param Capacity
/// \return Queue
Queue* QUEUE_create(u16 capacity); 

#endif //__ADT_QUEUE_H__