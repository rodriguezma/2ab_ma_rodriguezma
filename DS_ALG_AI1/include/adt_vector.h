// adt_vector.h
// Toni Barella
// Algoritmos & Inteligencia Artificial
// ESAT 2018/2019
//
#ifndef __ADT_VECTOR_H__
#define __ADT_VECTOR_H__

#include "adt_memory_node.h"

typedef struct adt_vector_s
{
	u16 head_;
	u16 tail_; 
	u16 capacity_;
	MemoryNode *storage_;
	struct vector_ops_s *ops_;
} Vector;

struct vector_ops_s
{

	/// \brief Destroy the vector, and its data
	/// \param Vector
	/// \return Error Code
	s16(*destroy) (Vector **vector);

	/// \brief Reset the data and the params
	/// \param Vector
	/// \return Error Code
	s16(*reset) (Vector *vector);		

	/// \brief Resize the vectors capacity
	/// \param Vector
	/// \param new size
	/// \return Error Code
	s16(*resize)(Vector *vector, u16 new_size); 

	/// \brief Returns the vectors capacity 
	/// \param Vector
	/// \return Capacity
	u16(*capacity)(Vector *vector);

	/// \brief Returns the vectors length 
	/// \param Vector
	/// \return Length
	u16(*length)(Vector *vector);

	/// \brief Checks if the vector is empty
	/// \param Vector
	/// \return true or false
	bool(*isEmpty) (Vector *vector);

	/// \brief Checks if the vector is full
	/// \param Vector
	/// \return true or false
	bool(*isFull) (Vector *vector);

	/// \brief Returns a pointer to the data of the first node
	/// \param Vector
	/// \return data
	void* (*first)(Vector *vector); 
	
	/// \brief Returns a pointer to the data of the last node
	/// \param Vector
	/// \return data
	void* (*last)(Vector *vector); 

	/// \brief Returns a pointer to the data of the given position
	/// \param Vector
	/// \param Position
	/// \return data
	void* (*at)(Vector *vector, u16 position); 

	/// \brief Inserts the data in the first position
	/// \param Vector
	/// \param Data
	/// \param Size
	/// \return Error Code
	s16(*insertFirst) (Vector *vector, void *data, u16 bytes);

	/// \brief Inserts the data in the last position
	/// \param Vector
	/// \param Data
	/// \param Size
	/// \return Error Code
	s16(*insertLast) (Vector *vector, void *data, u16 bytes); 

	/// \brief Inserts the data in the given position
	/// \param Vector
	/// \param Data
	/// \param Position
	/// \param Size
	/// \return Error Code
	s16(*insertAt) (Vector *vector, void *data, u16 position, u16 bytes); 

	// Extraction

	/// \brief Extracts the data of the first position
	/// \param Vector
	/// \return Data
	void*(*extractFirst) (Vector *vector);

	/// \brief Extracts the data of the last position
	/// \param Vector
	/// \return Data
	void*(*extractLast) (Vector *vector); 

	/// \brief Extracts the data of the given position
	/// \param Vector
	/// \return Data
	void*(*extractAt) (Vector *vector, u16 position); 

	/// \brief Concats the the two vectors in the first one
	/// \param Destination Vector
	/// \param Source Vector
	/// \return Error Code
	s16(*concat) (Vector *vector, Vector *vector_src); 

	/// \brief Executes the given function of the MN in all the nodes of the vector
	/// \param Vector
	/// \param Callback function
	/// \return Error Code
	s16(*traverse)(Vector *vector, void(*callback) (MemoryNode *));

	/// \brief Prints the data of the vector
	/// \param Vector
	void(*print)(Vector *vector); 
};

/// \brief Creates the vector with the given capacity
/// \param Capacity
/// \return Reference to the created vector
Vector* VECTOR_create(u16 capacity); 


#endif //__ADT_VECTOR_H__