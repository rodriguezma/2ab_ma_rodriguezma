// adt_list.h

#ifndef __ADT_DLLIST_H__
#define __ADT_DLLIST_H__

#include "adt_memory_node.h"

typedef struct adt_dllist_s
{
	MemoryNode *first_;
	MemoryNode *last_;
	u16 length_;
	u16 capacity_;
	struct dllist_ops_s *ops_;
} DLList;

struct dllist_ops_s
{


	/// \brief Destroy the list and set to NULL
	/// \param DLList 
	/// \return Error code
	s16(*destroy) (DLList **list);	

	/// \brief Reset the dllist, all the data is destroyed
	/// \param DLList
	/// \return Error code
	s16(*reset) (DLList *list);		

	/// \brief Resize the dllist. Irrelevant, dllist without capacity
	/// \param DLList 
	/// \return Error code
	s16(*resize)(DLList *list, u16 new_size); 

	/// \brief Capacity of the dllist, allways infinite, irrelevant
	/// \param DLList 
	/// \return Capacity
	u16(*capacity)(DLList *list);		

	/// \brief Lentgh of the dllist, number of nodes currently inserted
	/// \param DLList 
	/// \return Length
	u16(*length)(DLList *list);			
	
	/// \brief If the dllist doesnt have nodes
	/// \param DLList
	/// \return Boolean
	bool(*isEmpty) (DLList *list);
	
	/// \brief If the dllist is full, never happends, irrelevant
	/// \param DLList
	/// \return Boolean
	bool(*isFull) (DLList *list);

	/// \brief Retruns a reference to the data of the first node
	/// \param DLList
	/// \return Data
	void* (*first)(DLList *list); 
	
	/// \brief Retruns a reference to the data of the last node
	/// \param DLList
	/// \return Data
	void* (*last)(DLList *list); 
	
	/// \brief Returnsa reference to the data of the requested position
	/// \param DLList
	/// \return Data
	void* (*at)(DLList *list, u16 position); 

	/// \brief Inserts a node in the first position, with the given data
	/// \param DLList
	/// \param Data
	/// \param Data size
	/// \return Error code
	s16(*insertFirst) (DLList *list, void *data, u16 bytes); 
	
	/// \brief Inserts a node in the last position, with the given data
	/// \param DLList
	/// \param Data
	/// \param Data size
	/// \return Error code
	s16(*insertLast) (DLList *list, void *data, u16 bytes);
	
	/// \brief Inserts a node in the given position, with the given data
	/// \param DLList
	/// \param Data
	/// \param Position
	/// \param Data size
	/// \return Error code
	s16(*insertAt) (DLList *list, void *data, u16 position, u16 bytes); 
	
	/// \brief Inserts the given node in the last position
	/// \param DLList
	/// \param Node
	/// \return Error code
	s16(*insertNodeLast) (DLList *list, MemoryNode *node);
	
	/// \brief Extracts the data of the first node, destroys the node
	/// \param DLList
	/// \return Data
	void*(*extractFirst) (DLList *list); 
	
	/// \brief Extracts the data of the last node, destroys the node
	/// \param DLList
	/// \return Data
	void*(*extractLast) (DLList *list); 
	
	/// \brief Extracts the data of the given position node, destroys the node
	/// \param DLList
	/// \param Position
	/// \return Data
	void*(*extractAt) (DLList *list, u16 position); 

	/// \brief Concats the two lists in the first one
	/// \param Destination dllist
	/// \param Source dllist
	/// \return Error code
	s16(*concat) (DLList *list, DLList *list_src); 
	
	/// \brief Calls to the given memory node function in each node
	/// \param DLList
	/// \param Function callback
	/// \return Error code
	s16(*traverse)(DLList *list, void(*callback) (MemoryNode *)); 
	
	/// \brief Prints the dllist information
	/// \param DLList
	void(*print)(DLList *list); 
};

/// \brief Creates a dllist, and returns the reference. Irrelevant capacity.
/// \param Capacity
/// \return DLList
DLList* DLLIST_create(u16 capacity); 

#endif //__ADT_DLLIST_H__