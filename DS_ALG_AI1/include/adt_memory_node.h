
#ifndef __ADT_MEMORY_NODE_H__
#define __ADT_MEMORY_NODE_H__

#include "platform_types.h"

// Memory Node type
typedef struct memory_node_s
{
	void *data_;
	struct MemoryNode *prev_;
	struct MemoryNode *next_;
	u16 size_;
	struct memory_node_ops_s *ops_;
} MemoryNode;

// Memory Node's API Declarations
struct memory_node_ops_s
{
	/// \brief Returns a pointer to data
	/// \param Memory Node
	/// \return Pointer to data
	void*(*data) (MemoryNode *node);

	/// \brief Sets the value of data
	/// \param Memory Node
	/// \param Data source	
	/// \param Size of data	
	/// \return Error code
	s16(*setData) (MemoryNode *node, void *src, u16 bytes);

	/// \brief Returns data size
	/// \param Memory Node
	/// \return Data size
	u16(*size) (MemoryNode *node);

	/// \brief Resets the Memory Node, the data and its params
	/// \param Memory Node
	/// \return Pointer to data
	s16(*reset) (MemoryNode *node);	

	/// \brief Resets the params of the Memory Node
	/// \param Memory Node
	/// \return Error code
	s16(*softReset) (MemoryNode *node);

	/// \brief Free the MN, the data and itself
	/// \param Memory Node
	/// \return Error code
	s16(*free) (MemoryNode *node);

	/// \brief Free the MN, but not its data
	/// \param Memory Node
	/// \return Error code
	s16(*softFree) (MemoryNode *node);	

	/// \brief Sets the pointer next to the other MN
	/// \param Memory Node
	/// \param Next Memory Node
	/// \return Error code
	s16(*setNext)(MemoryNode *node, MemoryNode **next);

	/// \brief Sets the pointer prev to the other MN
	/// \param Memory Node
	/// \param Prev Memory Node
	/// \return Error code
	s16(*setPrev)(MemoryNode *node, MemoryNode **prev);

	/// \brief Sets the pointer next to the other MN
	/// \param Memory Node
	/// \param Next Memory Node
	/// \return Error code
	MemoryNode*(*next)(MemoryNode *node);

	/// \brief Returns the pointer to the next MN
	/// \param Memory Node
	/// \return Next Memory Node
	MemoryNode*(*prev)(MemoryNode *node);

	/// \brief Sets the data of the MN to the given value
	/// \param Memory Node
	/// \param value
	/// \return Error code
	s16(*memSet) (MemoryNode *node, u8 value);

	/// \brief Copy the memory of src to the MNs data
	/// \param Memory Node
	/// \param source
	/// \param Size
	/// \return Error code
	s16(*memCopy) (MemoryNode *node, void *src, u16 bytes);

	/// \brief Concats the MNs data with the given source
	/// \param Memory Node
	/// \param source
	/// \param Size
	/// \return Error code
	s16(*memConcat) (MemoryNode *node, void *src, u16 bytes);

	/// \brief Masks the MNs data with the given mask
	/// \param Memory Node
	/// \param Mask
	/// \return Error code
	s16(*memMask) (MemoryNode *node, u8 mask);

	/// \brief Prints the MNs data
	/// \param Memory Node
	void(*print) (MemoryNode *node);
};

/// \brief Create a MN and returns its reference
/// \return Memory Node
MemoryNode* MEMNODE_create();

/// \brief Init the given MN
/// \param Memory Node
/// \return Error Code
s16 MEMNODE_initWithoutCheck(MemoryNode *node);

/// \brief Create MN and sets to the given MN
/// \param Memory Node
s16 MEMNODE_createFromRef(MemoryNode **node);


#endif // __ADT_MEMORY_NODE_H__