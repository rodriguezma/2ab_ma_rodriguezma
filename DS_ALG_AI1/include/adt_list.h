// adt_list.h

#ifndef __ADT_LIST_H__
#define __ADT_LIST_H__

#include "adt_memory_node.h"

typedef struct adt_list_s
{
	MemoryNode *first_;
	MemoryNode *last_;
	u16 length_;
	u16 capacity_;
	struct list_ops_s *ops_;
} List;

struct list_ops_s
{
	/// \brief Destroy the list and set to NULL
	/// \param List 
	/// \return Error code
	s16(*destroy) (List **list);	

	/// \brief Reset the list, all the data is destroyed
	/// \param List
	/// \return Error code
	s16(*reset) (List *list);		

	/// \brief Resize the list. Irrelevant, list without capacity
	/// \param List 
	/// \return Error code
	s16(*resize)(List *list, u16 new_size); 

	/// \brief Capacity of the list, allways infinite, irrelevant
	/// \param List 
	/// \return Capacity
	u16(*capacity)(List *list);		

	/// \brief Lentgh of the list, number of nodes currently inserted
	/// \param List 
	/// \return Length
	u16(*length)(List *list);			

	/// \brief If the list doesnt have nodes
	/// \param List
	/// \return Boolean
	bool(*isEmpty) (List *list);

	/// \brief If the list is full, never happends, irrelevant
	/// \param List
	/// \return Boolean
	bool(*isFull) (List *list);

	/// \brief Retruns a reference to the data of the first node
	/// \param List
	/// \return Data
	void* (*first)(List *list); 
	
	/// \brief Retruns a reference to the data of the last node
	/// \param List
	/// \return Data
	void* (*last)(List *list); 
	
	/// \brief Returns a reference to the data of the requested position
	/// \param List
	/// \return Data
	void* (*at)(List *list, u16 position);

	/// \brief Inserts a node in the first position, with the given data
	/// \param List
	/// \param Data
	/// \param Data size
	/// \return Error code
	s16(*insertFirst) (List *list, void *data, u16 bytes); // Inserts an element in the first position of the list
	
	/// \brief Inserts a node in the last position, with the given data
	/// \param List
	/// \param Data
	/// \param Data size
	/// \return Error code
	s16(*insertLast) (List *list, void *data, u16 bytes); // Inserts an element in the last position of the list
	
	/// \brief Inserts a node in the given position, with the given data
	/// \param List
	/// \param Data
	/// \param Position
	/// \param Data size
	/// \return Error code
	s16(*insertAt) (List *list, void *data, u16 position, u16 bytes); // Inserts an element at the given position of the list
	
	/// \brief Inserts the given node in the last position
	/// \param List
	/// \param Node
	/// \return Error code
	s16(*insertNodeLast) (List *list, MemoryNode *node);
	
	/// \brief Extracts the data of the first node, destroys the node
	/// \param List
	/// \return Data
	void*(*extractFirst) (List *list); 

	/// \brief Extracts the data of the last node, destroys the node
	/// \param List
	/// \return Data
	void*(*extractLast) (List *list); 
	
	/// \brief Extracts the data of the given position node, destroys the node
	/// \param List
	/// \param Position
	/// \return Data
	void*(*extractAt) (List *list, u16 position); 

	/// \brief Concatenates the two lists in the first one
	/// \param Destination list
	/// \param Source list
	/// \return Error code
	s16(*concat) (List *list, List *list_src); 
	
	/// \brief Calls to the given memory node function in each node
	/// \param List
	/// \param Function callback
	/// \return Error code
	s16(*traverse)(List *list, void(*callback) (MemoryNode *)); // Calls to a function from all elements of the list
	
	/// \brief Prints the list information
	/// \param List
	void(*print)(List *list); // Print the features and content of the list
};

/// \brief Creates a list, and returns the reference. Irrelevant capacity.
/// \param Capacity
/// \return List
List* LIST_create(u16 capacity); // Create a new list

#endif //__ADT_LIST_H__